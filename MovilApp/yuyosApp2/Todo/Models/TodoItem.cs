﻿using SQLite;

namespace Todo
{
	public class TodoItem
	{
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string Nombre { get; set; }
		public string Codigo { get; set; }
		public bool Hecho { get; set; }
	}
}

