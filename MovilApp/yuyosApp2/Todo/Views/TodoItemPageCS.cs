﻿using Xamarin.Forms;

namespace Todo
{
	public class TodoItemPageCS : ContentPage
	{
		public TodoItemPageCS()
		{
			Title = "Registro Item";

			var nameEntry = new Entry();
			nameEntry.SetBinding(Entry.TextProperty, "Nombre");

			var notesEntry = new Entry();
			notesEntry.SetBinding(Entry.TextProperty, "Codigo");

			var doneSwitch = new Switch();
			doneSwitch.SetBinding(Switch.IsToggledProperty, "Hecho");

			var saveButton = new Button { Text = "Guardar" };
			saveButton.Clicked += async (sender, e) =>
			{
				var todoItem = (TodoItem)BindingContext;
				await App.Database.SaveItemAsync(todoItem);
				await Navigation.PopAsync();
			};

			var deleteButton = new Button { Text = "Borrar" };
			deleteButton.Clicked += async (sender, e) =>
			{
				var todoItem = (TodoItem)BindingContext;
				await App.Database.DeleteItemAsync(todoItem);
				await Navigation.PopAsync();
			};

			var cancelButton = new Button { Text = "Cancelar" };
			cancelButton.Clicked += async (sender, e) =>
			{
				await Navigation.PopAsync();
			};

			

			Content = new StackLayout
			{
				Margin = new Thickness(20),
				VerticalOptions = LayoutOptions.StartAndExpand,
				Children =
				{
					new Label { Text = "Nombre" },
					nameEntry,
					new Label { Text = "Codigo" },
					notesEntry,
					new Label { Text = "Hecho" },
					doneSwitch,
					saveButton,
					deleteButton,
					cancelButton
					
				}
			};
		}
	}
}
