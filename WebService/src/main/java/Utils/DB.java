package Utils;

import java.util.ArrayList;
import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;
import java.sql.*;

public class DB {

    private static Connection con = null;

    // Query to get data from DB (SELECT, FUNCTIONS)
    public CachedRowSet query(String query){
        try{
            //init db connection
            init();

            //create the statement object
            Statement stmt = con.createStatement();;

            //execute query
            ResultSet rs = stmt.executeQuery(query);

            //Create a disconnected object to handle results through components
            //ResultSet need a open connection, while CachedRowSet no
            RowSetFactory factory = RowSetProvider.newFactory();
            CachedRowSet crs = factory.createCachedRowSet();
            crs.populate(rs);

            //close the connection and ResultSet object
            con.close();
            rs.close();

            return crs;

        }catch (Exception e){
            System.out.println(e);
            return null;
        }
    }

    // Query to get modificated rows (INSERT, UPDATE, DELETE)
    public boolean rowQuery(String query, ArrayList data){
        try{
            // Init db connection
            init();

            // To get rows numbers, need use this statement
            // The query should have a '?' in the values you want to edit/insert/whatever
            PreparedStatement preparedStatement = con.prepareStatement(query);
            //System.out.println(query);

            // Add params to the statement, the long depends of the '?' at query
            // If it's more of less than the required, throw a exception at executeUpdate()
            for (int i = 0; i < data.size(); i++ ){
                preparedStatement.setString(i+1, data.get(i).toString());
            }
            //System.out.printf(preparedStatement.toString());
            /* Here the query it's send to DB and been executed. Returns the number of
            * updated rows.
            * Check if modificated rows are > 0. If you want, can get int */
            boolean result = preparedStatement.executeUpdate() > 0;

            // Close connection
            con.close();

            return  result;
        }catch (Exception e){
            System.out.println(e);
            return false;
        }
    }

    // For debug purposes
    public StringBuilder debugTableSelect(String query){
        CachedRowSet rs = query(query);
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("<table><tr>\n");
            //Get columns name
            for (int i = 1; i < rs.getMetaData().getColumnCount(); i++) {
                sb.append("\t<th>" + rs.getMetaData().getColumnName(i) + "</th>\n");
            }
            sb.append("</tr>\n");

            //Get the row
            while (rs.next()) {
                //Get columns data
                sb.append("<tr>\n");
                for (int i = 1; i < rs.getMetaData().getColumnCount(); i++) {
                    sb.append("\t<td>" + rs.getString(i) + "</td>\n");
                }
                sb.append("</tr>\n");
            }
            sb.append("</table>\n");

            rs.close();
            con.close();

            ///System.out.println(sb.toString());
            return sb;
        }catch (Exception e){
            System.out.println(e);
            return null;
        }
    }

    // Initialize the connection, but for the class use
    private boolean init(){
        try{
            //step1 load the driver class
            Class.forName("oracle.jdbc.driver.OracleDriver");

            //step2 create  the connection object
            //jdbc:oracle:thin:@192.168.47.130:1521:orcl
            con = DriverManager.getConnection(
                    "jdbc:oracle:thin:@yuyitos.cjsz6vvyagnp.us-west-2.rds.amazonaws.com:1521:orcl","yuyitos_admin","pass123");
            return true;

        }catch (Exception e){
            System.out.println(e);
            return false;
        }
    }
}

