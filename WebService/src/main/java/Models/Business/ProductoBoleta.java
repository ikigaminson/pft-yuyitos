package Models.Business;

import Models.Vendors.Productos;
import lombok.Data;

@Data
public class ProductoBoleta {
    private int id;
    private Boletas boleta;
    private Productos producto;
    private int cantidad;
    private int descuento;
    private int valor;
}

