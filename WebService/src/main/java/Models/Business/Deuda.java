package Models.Business;

import Models.Users.Clientes;
import lombok.Data;

import java.util.Date;

@Data
public class Deuda {
    private int id;
    private int monto;
    private String estado;
    private Date fechaPago;
    private Boletas boleta;
    private Clientes cliente;
    private char eliminado;
}
