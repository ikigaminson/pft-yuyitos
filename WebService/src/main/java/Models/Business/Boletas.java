package Models.Business;

import Models.Users.Clientes;
import Models.Users.Usuario;
import lombok.Data;

import java.sql.Date;

public @Data
class Boletas {
    private int id;
    private Date fecha;
    private int total;
    private String estado;
    private Clientes cliente;
    private Usuario usuario;
    private char eliminado;
}
