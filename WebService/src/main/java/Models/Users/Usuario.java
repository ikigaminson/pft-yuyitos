package Models.Users;

import lombok.Data;

@Data
public class Usuario {
    private int id;
    private String pass;
    private TipoUsuario tipoUsuario;
    private Persona persona;
    private char eliminado;

}
