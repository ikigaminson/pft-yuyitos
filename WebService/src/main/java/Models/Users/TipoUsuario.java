package Models.Users;

import lombok.Data;

@Data
public class TipoUsuario {
    private int id;
    private String nombreTipo;
    private char eliminado;
}
