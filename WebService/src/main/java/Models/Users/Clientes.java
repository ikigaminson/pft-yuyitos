package Models.Users;

import lombok.Data;

@Data
public class Clientes {
    private int id;
    private String estadoDeuda;
    private char fiado;
    private Persona persona;
    private char eliminado;
}

