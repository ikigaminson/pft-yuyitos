package Models.Users;

import lombok.Data;

@Data
public class Persona {
    private int rut;
    private char dv;
    private String nombre;
    private String paterno;
    private String materno;
    private String direccion;
    private long fono;
    private String mail;
}
