package Models.Vendors;

import lombok.Data;

@Data
public class ProductoPedido {
    private Pedido pedido;
    private Productos producto;
    private int cantidad;
    private int precioUnitario;
    private char estado;
}
