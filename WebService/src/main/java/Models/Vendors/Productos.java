package Models.Vendors;

import lombok.Data;

@Data
public class Productos {

    private int id;
    private int cod;
    private String nombre;
    private int precioVenta;
    private int precioCompra;
    private int stock;
    private Proveedor proveedor;
    private Familia familia;
    private char eliminado;
}
