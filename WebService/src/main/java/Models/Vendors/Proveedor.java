package Models.Vendors;

import lombok.Data;

@Data
public class Proveedor {

    private int id;
    private String nombre;
    private int rut;
    private char dv;
    private int phone;
    private String direccion;
    private String email;
    private char eliminado;
}
