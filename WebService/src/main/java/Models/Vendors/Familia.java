package Models.Vendors;

import lombok.Data;

import java.util.ArrayList;

@Data
public class Familia {
    private int id;
    private String nombre;
    private String descripcion;
    private char eliminado;
}
