package Models.Vendors;

import lombok.Data;

import java.sql.Date;

@Data
public class Pedido {
    private int id;
    private int valor;
    private Date fechaPedido;
    private Date fechaEstimada;
    private Date fechaRecepcion;
    private char eliminado;
}
