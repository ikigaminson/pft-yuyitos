package Models.Vendors;

import lombok.Data;

@Data
public class RubroProveedor {
    private int id;
    private Rubro rubro;
    private Proveedor proveedor;
}
