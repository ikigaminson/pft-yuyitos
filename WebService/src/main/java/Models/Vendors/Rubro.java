package Models.Vendors;

import lombok.Data;

@Data
public class Rubro {
    private int id;
    private String nombre;
    private char eliminado;
}
