import Models.Vendors.Productos;
import Models.Vendors.Proveedor;

import static spark.Spark.*;
import Services.*;

public class Main {
    public static void main(String[] args) {
        get("/", (req, res) -> "Server running... ");

        AdministracionService administracionService = new AdministracionService();
        ClienteService clienteService = new ClienteService();
        PersonalService personalService = new PersonalService();
        StockService stockService = new StockService();
        VentasService ventasService = new VentasService();

        administracionService.init();
        clienteService.init();
        personalService.init();
        stockService.init();
        ventasService.init();


    }
}
