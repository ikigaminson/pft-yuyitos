package Services;

import Controllers.Users.ClientesController;
import Models.Users.Clientes;
import Models.Users.Persona;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.security.ntlm.Client;

import static spark.Spark.*;

public class ClienteService {
    public void init(){
        /*options("/*", (request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            response.header("Access-Control-Allow-Methods", "POST");
            System.out.println(request.headers());

            return "post";
        });*/

        /*
         *       CLIENTE URLS
         */


        post("/clientes/all/",(request, response) -> {
            System.out.println(request.headers());
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            return new Gson().toJson(ClientesController.findAll());
        });

        get("/clientes/all",(request, response) -> {
            System.out.println(request.headers());
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            return new Gson().toJson(ClientesController.findAll());
        });

        post("/clientes/insert",(request, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            Clientes c = new Clientes();
            ClientesController cc = new ClientesController();

            String body = request.body();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            Persona persona;
            try {
                persona = gson.fromJson(body, Persona.class);
                System.out.println(persona);
                c.setPersona(persona);

                return new Gson().toJson(cc.insert(c));
            }catch (Exception e){
                System.out.println(e);
                c = new Clientes();
                c.setPersona(new Persona());

                return new Gson().toJson(c);
            }

        });

        post("/clientes/delete/:id",(request, response) -> {
            response.type("*/*");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            ClientesController cc = new ClientesController();


            return cc.delete(request.params(":id"));//new Gson().toJson(ClientesController.findAll());
        });

        post("/clientes/update/:id",(request, response) -> {
            response.type("*/*");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            ClientesController cc = new ClientesController();
            Clientes c = new Clientes();

            String body = request.body();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            Persona persona;
             try {
                persona = gson.fromJson(body, Persona.class);
                System.out.println(persona);
                c.setPersona(persona);

                return new Gson().toJson(cc.update(request.params(":id"), c));
            }catch (Exception e){
                System.out.println(e);
                c = new Clientes();
                c.setPersona(new Persona());

                return new Gson().toJson(c);
            }
        });

        //TODO: activa fiado al cliente
        get("/clientes/activar/:rut",(request, response) -> {
            //System.out.println(request.headers());
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            return "Fiado habilitado para el rut:" + request.params("rut");//new Gson().toJson(ClientesController.findAll());
        });

        //TODO: desactiva fiado al cliente
        get("/clientes/desactivar/:rut",(request, response) -> {
            //System.out.println(request.headers());
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            return "Fiado habilitado para el rut:" + request.params("rut");//new Gson().toJson(ClientesController.findAll());
        });

    }
}
