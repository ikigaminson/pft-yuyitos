package Services;

import Controllers.Vendors.PedidoController;
import Controllers.Vendors.ProductoPedidoController;
import Controllers.Vendors.ProductosController;
import Controllers.Vendors.ProveedorController;
import Models.Vendors.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import static spark.Spark.get;
import static spark.Spark.post;

public class StockService {
    public void init(){
        ProductosController pc = new ProductosController();
        ProductoPedidoController pbc = new ProductoPedidoController();

        /*
         *       PRODUCTO URLS
         */
        get("/products", (req, res) -> {
            ProductosController product = new ProductosController();
            return "<html><body>" + product.listAll() + "</body></html>";
        });

        get("/producto/all", (req, res) -> {
            res.type("application/json");
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "POST");
            return new Gson().toJson(ProductosController.findall());
        });


        get("/producto/search/:id", (req, res) -> {
            return new Gson().toJson(ProductosController.findById(req.params("id")));
        });

        post("/producto/insert", (req, res) -> {
            Productos producto = new Productos();
            producto.setProveedor(new Proveedor());
            producto.setFamilia(new Familia());

            System.out.printf(new Gson().toJson(producto));

            String body = req.body();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();

            //System.out.println(gson.toJson(boleta));

            try {
                producto = gson.fromJson(body, Productos.class);
                return pc.insert(producto);
            }catch (Exception e){
                System.out.println(e);
                return false;
            }
        });

        //TODO: delete producto from id
        get("/producto/delete/:id", (req, res) -> {
            return "Producto n°: " + req.params("id") + " eliminado";
        });

        /*
         *       PEDIDOS URLS
         */
        //TODO: get pedido status
        get("/pedido/status/:id", (req, res) -> {
            return "Detalle simple de pedido n°: "+ req.params("id");
        });

        //TODO: get pedido details
        get("/pedido/detalles/:id", (req, res) -> {
            return "Productos del pedido n°: "+ req.params("id");
        });

        //TODO: anular pedido
        get("/pedido/anular/:id", (req, res) -> {
            return "Anulación de pedido n°: "+ req.params("id");
        });

        post("/pedido/insertar/orden", (req, res) -> {
            PedidoController pedidoc = new PedidoController();
            Pedido pedido;

            String body = req.body();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();

            //System.out.println(gson.toJson(boleta));

            try {
                pedido = gson.fromJson(body, Pedido.class);
                return pedidoc.insert(pedido);
            }catch (Exception e){
                System.out.println(e);
                return false;
            }
        });

        post("/pedido/insertar/productos", (req, res) -> {
            String body = req.body();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();

            ProductoPedido ppt = new ProductoPedido();
            ppt.setPedido(new Pedido());
            ppt.setProducto(new Productos());

            System.out.println(gson.toJson(ppt));

            try {
                ArrayList<ProductoPedido> productos= (ArrayList<ProductoPedido>) gson.fromJson(body,
                        new TypeToken<ArrayList<ProductoPedido>>() {}.getType());
                for(ProductoPedido pp : productos){
                    pbc.insert(pp);
                }
            }catch (Exception e){
                System.out.println(e);
                return false;
            }

            return true;
        });

        get("/pedido/all", (req, res) -> {
            res.type("application/json");
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "POST");
            return new Gson().toJson(pbc.listAll());
        });

    }
}
