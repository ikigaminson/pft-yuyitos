package Services;

import Controllers.Vendors.FamiliaController;
import Controllers.Vendors.ProveedorController;
import Controllers.Vendors.RubroController;
import com.google.gson.Gson;

import static spark.Spark.*;

public class AdministracionService {
    public void init(){
        options("/*", (request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            response.header("Access-Control-Allow-Methods", "POST");
            System.out.println(request.headers());

            return "post";
        });

        /*
        *       PROVEEDOR URLS
        */
        // This it's kind of old, but still works :)
        post("/proveedor/insert", (req, res) -> {
            ProveedorController proveedor = new ProveedorController();
            return proveedor.insertProveedor(req.queryParams("name"),  Integer.parseInt(req.queryParams("rut")),
                    req.queryParams("dv").charAt(0), Integer.parseInt(req.queryParams("phone")),
                    req.queryParams("direccion"), req.queryParams("email"));
        });

        get("/proveedor/all", (req, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            return new Gson().toJson(ProveedorController.findAll());
        });

        post("/proveedor/delete", (req, res) -> {
            ProveedorController proveedor = new ProveedorController();
            return proveedor.delete(req.queryParams("id"));
        });

        post("/proveedor/update", (req, res) -> {
            ProveedorController proveedor = new ProveedorController();
            return proveedor.insertProveedor(req.queryParams("name"),  Integer.parseInt(req.queryParams("rut")),
                    req.queryParams("dv").charAt(0), Integer.parseInt(req.queryParams("phone")),
                    req.queryParams("direccion"), req.queryParams("email"));
        });

        /*
         *       FAMILIA URLS
         */
        get("/familia/all", (req, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            return new Gson().toJson(FamiliaController.listAll());
        });

        post("/familia/delete", (req, res) -> {
            FamiliaController familiaController = new FamiliaController();
            return familiaController.delete(req.queryParams("id"));
        });

        post("/familia/insert", (req, res) -> {
            FamiliaController familiaController = new FamiliaController();
            return familiaController.insert(req.queryParams("name"), req.queryParams("desc"));
        });

        /*
         *       RUBRO URLS
         */
        get("/rubro/all", (req, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            return new Gson().toJson(RubroController.listAll());
        });

        post("/rubro/delete", (req, res) -> {
            RubroController rubroController = new RubroController();
            return rubroController.delete(req.queryParams("id"));
        });

        post("/rubro/insert", (req, res) -> {
            RubroController rubroController = new RubroController();
            return rubroController.insert(req.queryParams("name"));
        });


    }
}
