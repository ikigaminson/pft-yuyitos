package Services;

import Controllers.Users.UsuarioController;
import com.google.gson.Gson;

import static spark.Spark.get;
import static spark.Spark.post;

/*
*   This class contains all about personal users
*
* */
public class PersonalService {
    public void init(){

        /*
         *       USUARIOS URLS
         */
        post("/usuarios/all",(request, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            return new Gson().toJson(UsuarioController.findAll());
        });


        get("/usuarios/debug/all",(request, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            return new Gson().toJson(UsuarioController.findAll());
        });

        post("/usuarios/login", (request, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            return UsuarioController.login(request.queryParams("user"), request.queryParams("pass"));
        });

        post("/usuarios/login", (request, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            return UsuarioController.login(request.queryParams("user"), request.queryParams("pass"));
        });

        post("/usuarios/search", (request, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            return new Gson().toJson(UsuarioController.search(request.queryParams("rut")));
        });

        post("/usuarios/delete", (request, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            return UsuarioController.delete(request.queryParams("id"));
        });

        /*
         *   TODO: FIX FROM HERE TO DOWN
         *
         * */
        post("/usuarios/update", (request, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            return UsuarioController.update(request.queryParams("id"));
        });
    }
}
