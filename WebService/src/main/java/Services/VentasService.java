package Services;

import Controllers.Business.BoletasController;
import Controllers.Business.ProductoBoletaController;
import Models.Business.Boletas;
import Models.Business.ProductoBoleta;
import Models.Users.Clientes;
import Models.Users.Usuario;
import Models.Vendors.Productos;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static spark.Spark.before;
import static spark.Spark.post;

public class VentasService {
    public void init(){

        /*
         *       BOLETAS URLS
         */
        post("/ventas/boletas/all",(request, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            return new Gson().toJson(BoletasController.findAll());
        });

        post("/ventas/insertar/boleta", (request, response)-> {
            BoletasController bc = new BoletasController();
            Boletas boleta;

            String body = request.body();//{"total": 500, "cliente": {"id":0}, "usuario": {"id": 1}}
            Gson gson = new GsonBuilder().setPrettyPrinting().create();

            //System.out.println(gson.toJson(boleta));

            try {
                boleta = gson.fromJson(body, Boletas.class);
                return bc.insert(boleta);
            }catch (Exception e){
                System.out.println(e);
                return false;
            }
        });

        post("/ventas/insertar/productos", (request, response)-> {
            ProductoBoletaController pbc = new ProductoBoletaController();

            String body = request.body();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();

            //System.out.println(gson.toJson(boleta));

            try {
                ArrayList<ProductoBoleta> productos= (ArrayList<ProductoBoleta>) gson.fromJson(body,
                        new TypeToken<ArrayList<ProductoBoleta>>() {}.getType());
                for(ProductoBoleta pb : productos){
                    pbc.insert(pb);
                }

            }catch (Exception e){
                System.out.println(e);
                return false;
            }

            return true;
        });

        post("/ventas/anular/:id", (request, response)-> {
            //TODO: recover all products objects from FE to add
            BoletasController bc = new BoletasController();
            Boletas boleta;

            return "Anular boleta n°: " + request.params(":id");
        });


        /*
         *       DEUDAS URLS
         */

        //TODO: Get fiado status
        post("/fiado/estado/:rut", (request, response)-> {
            //TODO: recover all products objects from FE to add
            BoletasController bc = new BoletasController();
            Boletas boleta;

            return "Estado de deuda del rut: " + request.params(":rut");
        });

        //TODO: Pay status
        post("/fiado/pago/:rut", (request, response)-> {
            //TODO: recover all products objects from FE to add
            BoletasController bc = new BoletasController();
            Boletas boleta;

            return "Pago del rut: " + request.params(":rut");
        });
    }
}
