package Controllers.Business;

import Controllers.Users.UsuarioController;
import Models.Business.Boletas;
import Utils.DB;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.eclipse.jetty.websocket.api.util.QuoteUtil;

import javax.sql.rowset.CachedRowSet;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class BoletasController {
    @Getter @Setter
    private Boletas boleta;
    private static DB connection = new DB();

    @SneakyThrows
    public static List<Boletas> findAll(){
        String query =
                "SELECT b.ID_BOLETAS ID, b.ESTADO ESTADO, b.FECHA FECHA, b.TOTAL TOTAL, p.NOMBRE V_NOMBRE, p.PATERNO V_PATERNO, u.ID_USUARIO V_ID" +
                        "FROM boletas b " +
                        "JOIN USUARIO u ON b.USUARIO_ID_USUARIO = u.ID_USUARIO" +
                        "JOIN PERSONA p ON u.PERSONA_RUT = p.RUT";
        List<Boletas> boletasList = new ArrayList<>();
        CachedRowSet rs = connection.query(query);

        while (rs.next()) {
            Boletas boleta = new Boletas();
            UsuarioController uc = new UsuarioController();

            boleta.getUsuario().setId(rs.getInt("V_ID"));
            boleta.getUsuario().getPersona().setNombre(rs.getString("V_NOMBRE"));
            boleta.getUsuario().getPersona().setNombre(rs.getString("V_PATERNO"));

            boleta.setId(rs.getInt("ID_BOLETAS"));
            boleta.setEstado(rs.getString("ESTADO"));
            boleta.setFecha(rs.getDate("FECHA"));
            boleta.setTotal(rs.getInt("TOTAL"));

            boletasList.add(boleta);
        }

        return boletasList;
    }

    @SneakyThrows
    public int insert(Boletas boleta){
        String query =
                "insert into BOLETAS (ESTADO, FECHA, TOTAL, USUARIO_ID_USUARIO) " +
                "values (?, CURRENT_DATE, ?, ?)";
        //fecha, total, estado, id_cliente, id_usuario

        String id_cliente = String.valueOf(boleta.getCliente().getId());

        ArrayList<String> data = new ArrayList<>();
        data.add(boleta.getEstado());
        data.add(String.valueOf(boleta.getTotal()));
        data.add(String.valueOf(boleta.getUsuario().getId()));

        if(connection.rowQuery(query,data)){
            if(id_cliente != "0")
                updateClient(String.valueOf(boleta.getUsuario().getId()), id_cliente);

            try {
                query = "SELECT max(ID_BOLETAS) lastID from BOLETAS";
                CachedRowSet rs = connection.query(query);
                int id = 0;
                while (rs.next()) {
                    id = rs.getInt("lastID");
                    break;

                }
                return id;

            }catch (Exception e){
                System.out.printf(e.getMessage());
                return -1;
            }
        }else
            return -2;//connection.rowQuery(query,data);
    }

    private boolean updateClient(String idBoleta, String idCliente){
        String query = "UPDATE BOLETAS " +
                "SET ID_CLIENTE = ? " +
                "WHERE ID_BOLETA = ?";

        ArrayList<String> data = new ArrayList<>();
        data.add(String.valueOf(idCliente));
        data.add(String.valueOf(idBoleta));

        return connection.rowQuery(query, data);
    }
}

/*
insert into BOLETAS (ID_BOLETAS, CLIENTES_ID_CLIENTE, ESTADO, FECHA, TOTAL, USUARIO_ID_USUARIO)
values (:CLIENTES_ID_CLIENTE, :ESTADO, :FECHA, :TOTAL, :USUARIO_ID_USUARIO)
;
 */
