package Controllers.Business;

import Models.Business.ProductoBoleta;
import Utils.DB;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class ProductoBoletaController {
    @Getter @Setter
    private static DB connection = new DB();

    public static ArrayList<ProductoBoleta> listall(){
        return new ArrayList<ProductoBoleta>();
    }

    public static boolean insert(ProductoBoleta pb){
        String query = "insert into PRODUCTO_BOLETA (BOLETAS_ID_BOLETAS, CANTIDAD, DESCUENTO, PRODUCTO_ID_PRODUCTO, VALOR) " +
                "values (?, ?, ?, ?, ?)";
        ArrayList<String> data = new ArrayList<>();

        data.add(String.valueOf(pb.getBoleta().getId()));
        data.add(String.valueOf(pb.getCantidad()));
        data.add(String.valueOf(pb.getDescuento()));
        data.add(String.valueOf(pb.getProducto().getId()));
        data.add(String.valueOf(pb.getValor()));

        return connection.rowQuery(query, data);
    }
}

