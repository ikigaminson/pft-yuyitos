package Controllers.Business;

import java.util.Date;

public class DeudaController {
    private int id;
    private int monto;
    private String estado;
    private Date fechaPago;
    private int idBoleta;
    private int idCliente;
    private char eliminado;

    public DeudaController(int id, int monto, String estado, Date fechaPago, int idBoleta, int idCliente, char eliminado) {
        this.id = id;
        this.monto = monto;
        this.estado = estado;
        this.fechaPago = fechaPago;
        this.idBoleta = idBoleta;
        this.idCliente = idCliente;
        this.eliminado = eliminado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMonto() {
        return monto;
    }

    public void setMonto(int monto) {
        this.monto = monto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public int getIdBoleta() {
        return idBoleta;
    }

    public void setIdBoleta(int idBoleta) {
        this.idBoleta = idBoleta;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public char getEliminado() {
        return eliminado;
    }

    public void setEliminado(char eliminado) {
        this.eliminado = eliminado;
    }
}
