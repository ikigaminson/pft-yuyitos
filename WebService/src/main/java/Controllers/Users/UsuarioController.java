package Controllers.Users;

import Models.Users.Usuario;
import Utils.DB;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;
import java.util.List;

public class UsuarioController {
    @Getter @Setter
    private Usuario usuario;
    private static DB connection = new DB();

    @SneakyThrows
    public static List<Usuario> findAll(){
        String query = "SELECT * FROM USUARIO";
        List<Usuario> usuarioList = new ArrayList<>();
        CachedRowSet rs = connection.query(query);

        while (rs.next()) {
            Usuario user = new Usuario();
            PersonaController pc = new PersonaController();
            TipoUsuarioController tuc = new TipoUsuarioController();

            //Get data from inner classes
            tuc.getTipoUsuario().setId(rs.getInt("ID_USUARIO"));
            pc.getPersona().setRut(rs.getInt("PERSONA_RUT"));//Oops, I did it again

            user.setEliminado(rs.getString("ELIMINADO").charAt(0));
            user.setId(rs.getInt("ID_USUARIO"));
            user.setTipoUsuario(tuc.get());
            user.setPersona(pc.get());

            usuarioList.add(user);
        }

        return usuarioList;
    }

    public static boolean login(String user, String pass){
        try {
            String query = "SELECT * FROM USUARIO WHERE " +
                    "PERSONA_RUT = '" + user + "' AND CLAVE = '" + pass + "'";
            CachedRowSet rs = connection.query(query);

            while (rs.next()) {
                return true;
            }
        }catch (Exception e){
            System.out.println(e);
        }
        return false;
    }

    @SneakyThrows
    public static boolean delete(String id){
        String query = "UPDATE usuario SET eliminado = 1 WHERE id_usuario = <?>" ;
        ArrayList<String> data = new ArrayList<>();
        data.add(id);

        return connection.rowQuery(query, data);
    }

    @SneakyThrows
    public static boolean update(String id){
        //TODO: CHANGE TO UPDATE USER
        String query = "UPDATE usuario SET eliminado = 1 WHERE id_usuario = <?>" ;
        ArrayList<String> data = new ArrayList<>();
        data.add(id);

        return connection.rowQuery(query, data);
    }

    @SneakyThrows
    public static Usuario search(String rut){
        String query = "SELECT * FROM USUARIO WHERE persona_rut = " + rut ;
        CachedRowSet rs = connection.query(query);
        Usuario user = new Usuario();

        while (rs.next()) {
            PersonaController pc = new PersonaController();
            TipoUsuarioController tuc = new TipoUsuarioController();

            //Get data from inner classes
            tuc.getTipoUsuario().setId(rs.getInt("ID_USUARIO"));
            pc.getPersona().setRut(rs.getInt("PERSONA_RUT"));//Oops, I did it again

            user.setEliminado(rs.getString("ELIMINADO").charAt(0));
            user.setId(rs.getInt("ID_USUARIO"));
            user.setTipoUsuario(tuc.get());
            user.setPersona(pc.get());

            break;
        }

        return user;
    }
}
