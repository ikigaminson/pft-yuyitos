package Controllers.Users;

import Models.Users.Clientes;
import Models.Users.Persona;
import Utils.DB;
import lombok.*;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;
import java.util.List;

public class ClientesController {
    @Getter @Setter
    private Clientes cliente;
    private static DB connection = new DB();

    @SneakyThrows
    public static List<Clientes> findAll(){
        String query = "SELECT * FROM clientes c JOIN PERSONA p ON p.RUT = c.PERSONA_RUT WHERE eliminado = 0";
        List<Clientes> clientesList = new ArrayList<>();
        CachedRowSet rs = connection.query(query);

        while (rs.next()) {
            Clientes client = new Clientes();
            PersonaController pc = new PersonaController();

            pc.getPersona().setRut(rs.getInt("PERSONA_RUT"));
            pc.getPersona().setDv(rs.getString("DV").charAt(0));
            pc.getPersona().setNombre(rs.getString("NOMBRE"));
            pc.getPersona().setPaterno(rs.getString("PATERNO"));
            pc.getPersona().setMaterno(rs.getString("MATERNO"));
            pc.getPersona().setFono(rs.getLong("FONO"));
            pc.getPersona().setDireccion(rs.getString("DIRECCION"));

            client.setEliminado(rs.getString("ELIMINADO").charAt(0));
            client.setEstadoDeuda(rs.getString("ESTADO_DEUDA"));
            client.setFiado(rs.getString("FIADO").charAt(0));
            client.setId(rs.getInt("ID_CLIENTE"));
            client.setPersona(pc.get());

            clientesList.add(client);
        }

        return clientesList;
    }

    @SneakyThrows
    public Clientes insert(Clientes c){
        PersonaController pc = new PersonaController();

        if(pc.insert(c.getPersona())) {
            ArrayList<String> data = new ArrayList<>();
            String query = "insert into CLIENTES" +
                    "(ESTADO_DEUDA,FIADO,PERSONA_RUT)" +
                    "VALUES (?, ?, ?)";

            data.add("P");
            data.add("0");
            data.add(String.valueOf(c.getPersona().getRut()));

            if(!connection.rowQuery(query, data))
                System.out.println("Pos que no ingresa");
        }

        return getLastInsert();
    }

    @SneakyThrows
    public Clientes getLastInsert(){
        Clientes client = new Clientes();

        // First gets id
        String sql = "SELECT max(id_column) last_id from CLIENTES";
        CachedRowSet rs = connection.query(sql);
        int id = 0;

        while (rs.next()) {
            id = rs.getInt("last_id");
            break;
        }

        // Second, gets object
        String query = "SELECT * FROM clientes c JOIN PERSONA p ON p.RUT = c.PERSONA_RUT WHERE ID_CLIENTE = " + id;

        while (rs.next()) {
            PersonaController pc = new PersonaController();

            pc.getPersona().setRut(rs.getInt("PERSONA_RUT"));
            pc.getPersona().setDv(rs.getString("DV").charAt(0));
            pc.getPersona().setNombre(rs.getString("NOMBRE"));
            pc.getPersona().setPaterno(rs.getString("PATERNO"));
            pc.getPersona().setMaterno(rs.getString("MATERNO"));
            pc.getPersona().setFono(rs.getLong("FONO"));
            pc.getPersona().setDireccion(rs.getString("DIRECCION"));

            client.setEliminado(rs.getString("ELIMINADO").charAt(0));
            client.setEstadoDeuda(rs.getString("ESTADO_DEUDA"));
            client.setFiado(rs.getString("FIADO").charAt(0));
            client.setId(rs.getInt("ID_CLIENTE"));
            client.setPersona(pc.get());
            break;

        }

        return client;
    }

    public boolean delete(String id){
        ArrayList<String> data = new ArrayList<>();
        String query = "update CLIENTES set ELIMINADO = 1 where ID_CLIENTE = ?";

        data.add(id);

        return connection.rowQuery(query, data);
    }

    public boolean update(String id, Clientes c){
        PersonaController pc = new PersonaController();

        return pc.update(c.getPersona());

        //This should update the cliente, but i notice we don't really change the
        //cliente data, just the persona data
        /*
        if(pc.update(c.getPersona())) {
            ArrayList<String> data = new ArrayList<>();
            String query = "UPDATE CLIENTES" +
                    "SET (ESTADO_DEUDA,FIADO,PERSONA_RUT)" +
                    "VALUES (?, ?, ?)";

            data.add("P");
            data.add("0");
            data.add(String.valueOf(c.getPersona().getRut()));

            return connection.rowQuery(query, data);

            /*if(!connection.rowQuery(query, data))
                System.out.println("Pos que no ingresa");
        }*/

    }
}