package Controllers.Users;

import Models.Users.TipoUsuario;
import Utils.DB;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

import javax.sql.rowset.CachedRowSet;

public class TipoUsuarioController {
    @Getter @Setter
    private TipoUsuario tipoUsuario = new TipoUsuario();
    private DB connection = new DB();

    @SneakyThrows
    public TipoUsuario get(){
        String query = "SELECT * FROM TIPO_USUARIO WHERE ID_TIPO = " + tipoUsuario.getId();
        CachedRowSet rs = connection.query(query);
        TipoUsuario tp = new TipoUsuario();

        while (rs.next()) {
            tp.setId(this.tipoUsuario.getId());
            tp.setEliminado(rs.getString("ELIMINADO").charAt(0));
            tp.setNombreTipo(rs.getString("NOMBRE_TIPO"));
            break;
        }

        return tp;
    }
}