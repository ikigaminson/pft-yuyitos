package Controllers.Users;

import Models.Users.Persona;
import Utils.DB;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;
import java.util.List;

public class PersonaController {
    @Getter @Setter
    private Persona persona = new Persona();
    private static DB connection = new DB();

    @SneakyThrows
    public Persona get(){
        String query = "SELECT * FROM persona WHERE rut = " + persona.getRut();
        CachedRowSet rs = connection.query(query);
        Persona persona = new Persona();

        while (rs.next()) {
            persona.setRut(this.persona.getRut());
            persona.setNombre(rs.getString("NOMBRE"));
            persona.setMaterno(rs.getString("MATERNO"));
            persona.setPaterno(rs.getString("PATERNO"));
            persona.setDireccion(rs.getString("DIRECCION"));
            persona.setDv(rs.getString("DV").charAt(0));
            persona.setFono(rs.getLong("FONO"));
            persona.setMail(rs.getString("MAIL"));
            break;
        }

        return persona;
    }

    @SneakyThrows
    public static List<Persona> getAll(){
        String query = "SELECT * FROM persona";
        List<Persona> personaList = new ArrayList<Persona>();
        CachedRowSet rs = connection.query(query);

        while (rs.next()) {
            Persona persona = new Persona();

            persona.setRut(rs.getInt("RUT"));
            persona.setNombre(rs.getString("NOMBRE"));
            persona.setMaterno(rs.getString("MATERNO"));
            persona.setPaterno(rs.getString("PATERNO"));
            persona.setDireccion(rs.getString("DIRECCION"));
            persona.setDv(rs.getString("DV").charAt(0));
            persona.setFono(rs.getLong("FONO"));
            persona.setMail(rs.getString("MAIL"));

            personaList.add(persona);
        }

        return personaList;
    }

    @SneakyThrows
    public boolean insert(Persona p){
        ArrayList<String> data = new ArrayList<>();
        String query = "insert into PERSONA" +
                "(RUT, DV, DIRECCION, FONO,MAIL,MATERNO,NOMBRE,PATERNO)" +
                "values (?,?,?,?,?,?,?,?)";

        data.add(String.valueOf(p.getRut()));
        data.add(String.valueOf(p.getDv()));
        data.add(p.getDireccion());
        data.add(String.valueOf(p.getFono()));
        data.add(p.getMail());
        data.add(p.getMaterno());
        data.add(p.getNombre());
        data.add(p.getPaterno());

        return connection.rowQuery(query, data);
    }

    @SneakyThrows
    public boolean update(Persona p){
        ArrayList<String> data = new ArrayList<>();
        String query = "UPDATE PERSONA SET " +
                "DIRECCION = ?," +
                "FONO = ?," +
                "MAIL = ?," +
                "MATERNO = ?," +
                "NOMBRE = ?," +
                "PATERNO = ?" +
                "WHERE RUT = ?";

        data.add(p.getDireccion());
        data.add(String.valueOf(p.getFono()));
        data.add(p.getMail());
        data.add(p.getMaterno());
        data.add(p.getNombre());
        data.add(p.getPaterno());
        data.add(String.valueOf(p.getRut()));

        return connection.rowQuery(query, data);
    }

    @SneakyThrows
    public boolean delete(String id){
        ArrayList<String> data = new ArrayList<>();
        String query = "UPDATE PERSONA SET ELIMIN WHERE rut = ?";

        data.add(id);

        return connection.rowQuery(query, data);
    }

    @SneakyThrows
    public Persona search(String rut){
        String query = "SELECT * FROM persona WHERE rut = " + rut ;
        CachedRowSet rs = connection.query(query);
        Persona persona = new Persona();

        while (rs.next()) {
            persona.setRut(rs.getInt("RUT"));
            persona.setNombre(rs.getString("NOMBRE"));
            persona.setMaterno(rs.getString("MATERNO"));
            persona.setPaterno(rs.getString("PATERNO"));
            persona.setDireccion(rs.getString("DIRECCION"));
            persona.setDv(rs.getString("DV").charAt(0));
            persona.setFono(rs.getLong("FONO"));
            persona.setMail(rs.getString("MAIL"));

            break;
        }

        return persona;
    }
}
