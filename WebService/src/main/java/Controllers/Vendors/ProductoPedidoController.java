package Controllers.Vendors;

import Models.Vendors.ProductoPedido;
import Models.Vendors.Productos;
import Utils.DB;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

public class ProductoPedidoController {
    @Getter @Setter
    public ProductoPedido productoPedido;

    public boolean insert(ProductoPedido p){//String valor, String fechaPedido, String fechaEstimada){
        try{
            DB connection = new DB();
            String query = "insert into PRODUCTO_PEDIDO (PEDIDO_ID_PEDIDO,PRODUCTO_ID_PRODUCTO,CANTIDAD,ESTADO,PRECIO_UNITARIO) " +
                    "values (?,?,?,?,?)";

            ArrayList<String> data = new ArrayList<>();
            data.add(String.valueOf(p.getPedido().getId()));
            data.add(String.valueOf(p.getProducto().getId()));
            data.add(String.valueOf(p.getCantidad()));
            data.add(String.valueOf(p.getEstado()));
            data.add(String.valueOf(p.getPrecioUnitario()));

            return connection.rowQuery(query,data);
        }catch (Exception e){
            System.out.println(e);
            return false;
        }
    }

    public StringBuilder listAll(){

        DB connection = new DB();
        String query = "select * " +
                "from PEDIDO";

        return connection.debugTableSelect(query);
    }
}
/*
insert into PRODUCTO_PEDIDO (PEDIDO_ID_PEDIDO,PRODUCTO_ID_PRODUCTO,CANTIDAD,ESTADO,PRECIO_UNITARIO)
values (?,?,?,?,?)
;
 */