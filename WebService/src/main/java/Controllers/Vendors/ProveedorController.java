package Controllers.Vendors;

import Models.Vendors.Proveedor;
import Utils.DB;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;
import java.util.List;

public class ProveedorController {
    @Getter @Setter
    private Proveedor proveedor = new Proveedor();
    private static DB connection = new DB();

    @SneakyThrows
    public static Proveedor get(String id){
        Proveedor p = new Proveedor();
        String query = "select * " +
                "from PROVEEDOR WHERE ID_PROVEEDOR = " + id;
        CachedRowSet rs = connection.query(query);
        while (rs.next()){
            p.setId(rs.getInt("ID_PROVEEDOR"));
            p.setDireccion(rs.getString("DIRECCION"));
            p.setNombre(rs.getString("NOMBRE"));
            p.setEmail(rs.getString("MAIL"));
            p.setPhone(rs.getInt("TELEFONO"));
            return p;
        }
        return null;
    }

    @SneakyThrows
    public static List<Proveedor> findAll(){
        String query = "SELECT * FROM proveedor";
        List<Proveedor> proveedorList = new ArrayList<Proveedor>();
        CachedRowSet rs = connection.query(query);

        while (rs.next()) {
            Proveedor proveedor = new Proveedor();

            proveedor.setId(rs.getInt("ID_PROVEEDOR"));
            proveedor.setNombre(rs.getString("NOMBRE"));
            proveedor.setRut(rs.getInt("RUT_EMPRESA"));
            proveedor.setDv(rs.getString("DV_EMPRESA").charAt(0));
            proveedor.setPhone(rs.getInt("TELEFONO"));
            proveedor.setDireccion(rs.getString("DIRECCION"));
            proveedor.setEmail(rs.getString("MAIL"));
            proveedor.setEliminado(rs.getString("ELIMINADO").charAt(0));

            proveedorList.add(proveedor);
        }

        return proveedorList;
    }

    // Logical delete
    public boolean delete(String id){
        String query = "UPDATE PROVEEDOR SET eliminado = 1 WHERE ID_PROVEEDOR = ?";

        ArrayList<String> data = new ArrayList<>();
        data.add(id);

        return connection.rowQuery(query, data);
    }

    @SneakyThrows
    public boolean insertProveedor(String name, int rut, char dv, int phone, String direction, String mail){
            String query = "INSERT INTO proveedor " +
                    "(NOMBRE, RUT_EMPRESA, DV_EMPRESA, TELEFONO, DIRECCION, MAIL)" +
                    "VALUES (?, ?, ?, ?, ?, ?)";

            ArrayList<String> data = new ArrayList<>();
            data.add(name);
            data.add(String.valueOf(rut));
            data.add(String.valueOf(dv));
            data.add(String.valueOf(phone));
            data.add(direction);
            data.add(mail);

            return connection.rowQuery(query, data);
    }

    public StringBuilder listAll(){

        DB connection = new DB();
        String query = "select * " +
                "from proveedor";

        return connection.debugTableSelect(query);
    }
}
