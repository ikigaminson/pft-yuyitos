package Controllers.Vendors;

import Models.Vendors.Rubro;
import Utils.DB;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;
import java.util.List;

public class RubroController {
    @Getter @Setter
    private static DB connection = new DB();
    private Rubro rubro = new Rubro();


    public boolean insert(String nombre){
        try{
            String query = "insert into RUBRO (ELIMINADO,NOMBRE)" +
                    "values (?, ?)";

            ArrayList<String> data = new ArrayList<>();
            data.add("0");
            data.add(nombre);

            return connection.rowQuery(query,data);
        }catch (Exception e){
            System.out.println(e);
            return false;
        }
    }

    @SneakyThrows
    public static List<Rubro> listAll(){
        String query = "select * " +
                "from RUBRO " +
                "where ELIMINADO = 0";
        List<Rubro> lstRubro = new ArrayList<>();
        CachedRowSet rs = connection.query(query);

        while (rs.next()) {
            Rubro rub = new Rubro();

            rub.setId(rs.getInt("ID_RUBRO"));
            rub.setNombre(rs.getString("NOMBRE"));

            lstRubro.add(rub);
        }

        return lstRubro;
    }

    @SneakyThrows
    public boolean delete(String id){
        String query = "UPDATE RUBRO SET " +
                "ELIMINADO = 1," +
                "WHERE ID_RUBRO = ?";

        ArrayList<String> data = new ArrayList<>();
        data.add(id);

        return connection.rowQuery(query, data);
    }
}
