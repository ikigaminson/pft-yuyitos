package Controllers.Vendors;

import Models.Vendors.Pedido;
import Utils.DB;
import lombok.SneakyThrows;

import javax.sql.rowset.CachedRowSet;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class PedidoController {
    private static DB connection = new DB();

    public int insert(Pedido p){//String valor, String fechaPedido, String fechaEstimada){
        try{
            String query = "INSERT INTO PEDIDO" +
                "(VALOR, FECHA_PEDIDO, FECHA_ESTIMADA, ELIMINADO)" +
                "VALUES (?, CURRENT_DATE, CURRENT_DATE + 5, ?)";

            ArrayList<String> data = new ArrayList<>();
            data.add(String.valueOf(p.getValor()));
            data.add("0");

            if(connection.rowQuery(query,data)){
                try {
                    query = "SELECT max(ID_PEDIDO) lastID from PEDIDO";
                    CachedRowSet rs = connection.query(query);
                    int id = 0;
                    while (rs.next()) {
                        id = rs.getInt("lastID");
                        break;

                    }
                    return id;

                }catch (Exception e){
                    System.out.printf(e.getMessage());
                    return -1;
                }
            }else
                return -2;
        }catch (Exception e){
            System.out.println(e);
            return -3;
        }
    }

    @SneakyThrows
    public ArrayList<Pedido> listAll(){
        String query = "SELECT max(ID_PEDIDO) lastID from PEDIDO";
        ArrayList<Pedido> pedidosList = new ArrayList<>();
        CachedRowSet rs = connection.query(query);

        while (rs.next()) {
            Pedido p = new Pedido();

            p.setId(rs.getInt(""));
            p.setValor(rs.getInt(""));
            p.setFechaPedido(rs.getDate(""));
            p.setFechaEstimada(rs.getDate(""));
            p.setFechaRecepcion(rs.getDate(""));

            pedidosList.add(p);
        }

        return pedidosList;
    }

    public StringBuilder listTable(){
        String query = "select * " +
                "from PEDIDO";

        return connection.debugTableSelect(query);
    }
}
