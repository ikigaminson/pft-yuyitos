package Controllers.Vendors;

import Models.Vendors.Familia;
import Utils.DB;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;
import java.util.List;

public class FamiliaController {
    @Getter @Setter
    private static DB connection = new DB();
    private Familia familia = new Familia();

    @SneakyThrows
    public boolean insert(String nombre, String descripcion){
        String query = "INSERT INTO FAMILIA " +
                "(NOMBRE, DESCRIPCION, ELIMINADO)" +
                "VALUES (?, ?, ?)";

        ArrayList<String> data = new ArrayList<>();
        data.add(nombre);
        data.add(descripcion);
        data.add("0");

        return connection.rowQuery(query,data);
    }

    @SneakyThrows
    public static List<Familia> listAll(){
        String query = "select * " +
                "from FAMILIA " +
                "where eliminado = 0";
        List<Familia> lstFamilia = new ArrayList<>();
        CachedRowSet rs = connection.query(query);

        while (rs.next()) {
            Familia fam = new Familia();

            fam.setId(rs.getInt("ID_FAMILIA"));
            fam.setNombre(rs.getString("NOMBRE"));
            fam.setDescripcion(rs.getString("DESCRIPCION"));

            lstFamilia.add(fam);
        }

        return lstFamilia;
    }

    @SneakyThrows
    public boolean delete(String id){
        String query = "UPDATE FAMILIA SET " +
                "ELIMINADO = 1," +
                "WHERE ID_FAMILIA = ?";

        ArrayList<String> data = new ArrayList<>();
        data.add(id);

        return connection.rowQuery(query, data);
    }
}
