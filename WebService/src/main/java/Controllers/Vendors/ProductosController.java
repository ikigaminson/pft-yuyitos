package Controllers.Vendors;
import Models.Vendors.Familia;
import Models.Vendors.Productos;
import Models.Vendors.Proveedor;
import Utils.DB;
import lombok.SneakyThrows;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;

public class ProductosController {
    private static DB connection = new DB();

    @SneakyThrows
    public static ArrayList<Productos> findall(){
        String query = "" +
                "select " +
                "p.ID_PRODUCTO as ID_PRODUCTO, " +
                "p.COD_PRODUCTO as COD_PRODUCTO, " +
                "p.NOMBRE as NOMBRE_PRODUCTO, " +
                "p.PRECIO_VENTA as PRECIO_VENTA, " +
                "p.STOCK as STOCK, " +
                "f.NOMBRE as NOMBRE_FAMILIA, " +
                "pr.NOMBRE as NOMBRE_PROVEEDOR " +
                "from producto p " +
                "JOIN FAMILIA f ON f.ID_FAMILIA = p.FAMILIA_ID_FAMILIA " +
                "JOIN PROVEEDOR pr ON pr.ID_PROVEEDOR = p.PROVEEDOR_ID_PROV " +
                "WHERE p.eliminado = 0";
        CachedRowSet rs = connection.query(query);
        ArrayList<Productos> listProducts = new ArrayList();

        while (rs.next()){
            Productos p = new Productos();
            Familia f = new Familia();
            Proveedor prov = new Proveedor();

            f.setNombre(rs.getString("NOMBRE_FAMILIA"));
            prov.setNombre(rs.getString("NOMBRE_PROVEEDOR"));

            p.setId(rs.getInt("ID_PRODUCTO"));
            p.setCod(rs.getInt("COD_PRODUCTO"));
            p.setNombre(rs.getString("NOMBRE_PRODUCTO"));
            p.setPrecioVenta(rs.getInt("PRECIO_VENTA"));
            p.setStock(rs.getInt("STOCK"));
            p.setFamilia(f);
            p.setProveedor(prov);

            listProducts.add(p);
        }

        return listProducts;
    }

   @SneakyThrows
    public static Productos findById(String id){
        Productos p = new Productos();
        String query = "select * " +
                "from producto WHERE ID_PRODUCTO = " + id;
        CachedRowSet rs = connection.query(query);
        while (rs.next()){
            p.setId(rs.getInt("ID_PRODUCTO"));
            p.setCod(rs.getInt("COD_PRODUCTO"));
            p.setNombre(rs.getString("NOMBRE"));
            p.setPrecioVenta(rs.getInt("PRECIO_VENTA"));
            p.setStock(rs.getInt("STOCK"));
            p.setProveedor(ProveedorController.get(rs.getString("PROVEEDOR_ID_PROV")));


            return p;
        }

        return null;
    }

    public boolean insert(Productos p){//String cod, String nombre, String precioVenta, String precioCompra, String stock, String idProveedor, String idFamilia){
        try{
            String query = "INSERT INTO PRODUCTO" +
                    "(COD_PRODUCTO, NOMBRE, PRECIO_VENTA, PRECIO_COMPRA, STOCK, PROVEEDOR_ID_PROV, FAMILIA_ID_FAMILIA, ELIMINADO)" +
                    "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";

            ArrayList<String> data = new ArrayList<>();
            data.add(String.valueOf(p.getCod()));
            data.add(p.getNombre());
            data.add(String.valueOf(p.getPrecioVenta()));
            data.add(String.valueOf(p.getPrecioCompra()));
            data.add(String.valueOf(p.getStock()));
            data.add(String.valueOf(p.getProveedor().getId()));
            data.add(String.valueOf(p.getFamilia().getId()));
            data.add("0");

            return connection.rowQuery(query, data);
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
    }

    /// Simple list products, just listed as HTML table
    ///
    public StringBuilder listAll(){
        String query = "select * " +
                "from producto";

        return connection.debugTableSelect(query);
    }


}
