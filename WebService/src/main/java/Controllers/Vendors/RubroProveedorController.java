package Controllers.Vendors;

import Utils.DB;

import java.util.ArrayList;

public class RubroProveedorController {
    private int id;
    private int idRubro;
    private int idProveedor;

    public RubroProveedorController(int id, int idRubro, int idProveedor) {
        this.id = id;
        this.idRubro = idRubro;
        this.idProveedor = idProveedor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdRubro() {
        return idRubro;
    }

    public void setIdRubro(int idRubro) {
        this.idRubro = idRubro;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public boolean insert(String idRubro, String idProveedor){
        try{
            DB connection = new DB();
            String query = "INSERT INTO RUBRO_PROVEEDOR " +
                    "(RUBRO_ID_RUBRO, PROVEEDOR_ID_PROV) " +
                    "VALUES (?,?)";

            ArrayList<String> data = new ArrayList<>();
            data.add(idRubro);
            data.add(idProveedor);

            return connection.rowQuery(query,data);
        }catch (Exception e){
            System.out.println(e);
            return false;
        }
    }

    public StringBuilder listAll(){

        DB connection = new DB();
        String query = "select * " +
                "from RUBRO_PROVEEDOR";

        return connection.debugTableSelect(query);
    }
}
