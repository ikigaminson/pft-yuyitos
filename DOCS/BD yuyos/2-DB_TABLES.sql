/*
 *  SCRIPT GENERACION DE TABLAS
 *  Generacion de tablas al 2019-04-07
 *
 */

-- Generated by Oracle SQL Developer Data Modeler 3.2.0.735
--   at:        2019-04-07 12:41:15 PDT
--   site:      Oracle Database 11g
--   type:      Oracle Database 11g



CREATE TABLE boletas 
    ( 
     id_boletas INTEGER  NOT NULL , 
     fecha DATE  NOT NULL , 
     total INTEGER  NOT NULL , 
     estado VARCHAR2 (2 CHAR)  NOT NULL , 
     clientes_id_cliente INTEGER , 
     usuario_id_usuario INTEGER  NOT NULL , 
     eliminado CHAR (1) DEFAULT 0  NOT NULL 
    ) 
;



ALTER TABLE boletas 
    ADD CONSTRAINT boletas_PK PRIMARY KEY ( id_boletas ) ;



CREATE TABLE clientes 
    ( 
     id_cliente INTEGER  NOT NULL , 
     estado_deuda VARCHAR2 (2 CHAR)  NOT NULL , 
     fiado CHAR (1) , 
     persona_rut INTEGER  NOT NULL , 
     eliminado CHAR (1) DEFAULT 0  NOT NULL 
    ) 
;



ALTER TABLE clientes 
    ADD CONSTRAINT clientes_PK PRIMARY KEY ( id_cliente ) ;



CREATE TABLE deuda 
    ( 
     id_deuda INTEGER  NOT NULL , 
     monto INTEGER  NOT NULL , 
     estado VARCHAR2 (2 CHAR)  NOT NULL , 
     fecha_pago DATE , 
     boletas_id_boletas INTEGER , 
     clientes_id_cliente INTEGER , 
     eliminado CHAR (1) DEFAULT 0  NOT NULL 
    ) 
;



ALTER TABLE deuda 
    ADD CONSTRAINT deuda_PK PRIMARY KEY ( id_deuda ) ;



CREATE TABLE familia 
    ( 
     id_familia INTEGER  NOT NULL , 
     nombre VARCHAR2 (25 CHAR)  NOT NULL , 
     descripcion VARCHAR2 (50 CHAR) , 
     eliminado CHAR (1) DEFAULT 0  NOT NULL 
    ) 
;



ALTER TABLE familia 
    ADD CONSTRAINT familia_PK PRIMARY KEY ( id_familia ) ;



CREATE TABLE pedido 
    ( 
     id_pedido INTEGER  NOT NULL , 
     valor INTEGER  NOT NULL , 
     fecha_pedido DATE  NOT NULL , 
     fecha_estimada DATE  NOT NULL , 
     fecha_recepcion DATE , 
     eliminado CHAR (1) DEFAULT 0  NOT NULL 
    ) 
;



ALTER TABLE pedido 
    ADD CONSTRAINT registro_pedido_PK PRIMARY KEY ( id_pedido ) ;



CREATE TABLE persona 
    ( 
     rut INTEGER  NOT NULL , 
     dv VARCHAR2 (1 CHAR)  NOT NULL , 
     nombre VARCHAR2 (20 CHAR)  NOT NULL , 
     paterno VARCHAR2 (30 CHAR)  NOT NULL , 
     materno VARCHAR2 (30 CHAR)  NOT NULL , 
     direccion VARCHAR2 (50 CHAR) , 
     fono INTEGER  NOT NULL , 
     mail VARCHAR2 (40 CHAR)  NOT NULL 
    ) 
    LOGGING 
;



ALTER TABLE persona 
    ADD CONSTRAINT persona_PK PRIMARY KEY ( rut ) ;



CREATE TABLE producto 
    ( 
     id_producto INTEGER  NOT NULL , 
     cod_producto INTEGER  NOT NULL , 
     nombre VARCHAR2 (30 CHAR)  NOT NULL , 
     precio_venta INTEGER  NOT NULL , 
     precio_compra INTEGER  NOT NULL , 
     stock INTEGER  NOT NULL , 
     proveedor_id_prov INTEGER  NOT NULL , 
     familia_id_familia INTEGER  NOT NULL , 
     eliminado CHAR (1) DEFAULT 0  NOT NULL 
    ) 
;



ALTER TABLE producto 
    ADD CONSTRAINT producto_PK PRIMARY KEY ( id_producto ) ;



CREATE TABLE producto_boleta 
    ( 
     id_row INTEGER  NOT NULL , 
     boletas_id_boletas INTEGER  NOT NULL , 
     producto_id_producto INTEGER  NOT NULL , 
     cantidad INTEGER  NOT NULL , 
     descuento INTEGER  NOT NULL , 
     valor INTEGER  NOT NULL 
    ) 
    LOGGING 
;



ALTER TABLE producto_boleta 
    ADD CONSTRAINT producto_boleta_PK PRIMARY KEY ( id_row ) ;


ALTER TABLE producto_boleta 
    ADD CONSTRAINT producto_boleta__UN UNIQUE ( boletas_id_boletas , producto_id_producto ) ;



CREATE TABLE producto_pedido 
    ( 
     pedido_id_pedido INTEGER  NOT NULL , 
     producto_id_producto INTEGER  NOT NULL , 
     cantidad INTEGER  NOT NULL , 
     precio_unitario INTEGER  NOT NULL , 
     estado CHAR (1) 
    ) 
    LOGGING 
;



ALTER TABLE producto_pedido 
    ADD CONSTRAINT producto_pedido_PK PRIMARY KEY ( pedido_id_pedido, producto_id_producto ) ;



CREATE TABLE proveedor 
    ( 
     id_proveedor INTEGER  NOT NULL , 
     nombre VARCHAR2 (50 CHAR)  NOT NULL , 
     rut_empresa INTEGER  NOT NULL , 
     dv_empresa VARCHAR2 (1 CHAR)  NOT NULL , 
     telefono INTEGER  NOT NULL , 
     direccion VARCHAR2 (50 CHAR)  NOT NULL , 
     mail VARCHAR2 (50 CHAR)  NOT NULL , 
     eliminado CHAR (1) DEFAULT 0  NOT NULL 
    ) 
;



ALTER TABLE proveedor 
    ADD CONSTRAINT proveedor_PK PRIMARY KEY ( id_proveedor ) ;



CREATE TABLE rubro 
    ( 
     id_rubro INTEGER  NOT NULL , 
     nombre VARCHAR2 (20)  NOT NULL , 
     eliminado CHAR (1) DEFAULT 0  NOT NULL 
    ) 
;



ALTER TABLE rubro 
    ADD CONSTRAINT rubro_PK PRIMARY KEY ( id_rubro ) ;



CREATE TABLE rubro_proveedor 
    ( 
     id_row INTEGER  NOT NULL , 
     rubro_id_rubro INTEGER  NOT NULL , 
     proveedor_id_prov INTEGER  NOT NULL 
    ) 
;



ALTER TABLE rubro_proveedor 
    ADD CONSTRAINT rubro_proveedor_PK PRIMARY KEY ( id_row ) ;



CREATE TABLE tipo_usuario 
    ( 
     id_tipo INTEGER  NOT NULL , 
     nombre_tipo VARCHAR2 (20 CHAR)  NOT NULL , 
     eliminado CHAR (1) DEFAULT 0  NOT NULL 
    ) 
;



ALTER TABLE tipo_usuario 
    ADD CONSTRAINT tipo_usuario_PK PRIMARY KEY ( id_tipo ) ;



CREATE TABLE usuario 
    ( 
     id_usuario INTEGER  NOT NULL , 
     clave VARCHAR2 (255 CHAR)  NOT NULL , 
     tipo_usuario_tipo INTEGER  NOT NULL , 
     persona_rut INTEGER  NOT NULL , 
     eliminado CHAR (1) DEFAULT 0  NOT NULL 
    ) 
;



ALTER TABLE usuario 
    ADD CONSTRAINT usuario_PK PRIMARY KEY ( id_usuario ) ;




ALTER TABLE boletas 
    ADD CONSTRAINT boletas_clientes_FKv2 FOREIGN KEY 
    ( 
     clientes_id_cliente
    ) 
    REFERENCES clientes 
    ( 
     id_cliente
    ) 
    NOT DEFERRABLE 
;


ALTER TABLE boletas 
    ADD CONSTRAINT boletas_usuario_FK FOREIGN KEY 
    ( 
     usuario_id_usuario
    ) 
    REFERENCES usuario 
    ( 
     id_usuario
    ) 
    NOT DEFERRABLE 
;


ALTER TABLE clientes 
    ADD CONSTRAINT clientes_persona_FK FOREIGN KEY 
    ( 
     persona_rut
    ) 
    REFERENCES persona 
    ( 
     rut
    ) 
    NOT DEFERRABLE 
;


ALTER TABLE deuda 
    ADD CONSTRAINT deuda_boletas_FK FOREIGN KEY 
    ( 
     boletas_id_boletas
    ) 
    REFERENCES boletas 
    ( 
     id_boletas
    ) 
    NOT DEFERRABLE 
;


ALTER TABLE deuda 
    ADD CONSTRAINT deuda_clientes_FK FOREIGN KEY 
    ( 
     clientes_id_cliente
    ) 
    REFERENCES clientes 
    ( 
     id_cliente
    ) 
    NOT DEFERRABLE 
;


ALTER TABLE producto_boleta 
    ADD CONSTRAINT producto_boleta_boletas_FK FOREIGN KEY 
    ( 
     boletas_id_boletas
    ) 
    REFERENCES boletas 
    ( 
     id_boletas
    ) 
    NOT DEFERRABLE 
;


ALTER TABLE producto_boleta 
    ADD CONSTRAINT producto_boleta_producto_FK FOREIGN KEY 
    ( 
     producto_id_producto
    ) 
    REFERENCES producto 
    ( 
     id_producto
    ) 
    NOT DEFERRABLE 
;


ALTER TABLE producto 
    ADD CONSTRAINT producto_familia_FK FOREIGN KEY 
    ( 
     familia_id_familia
    ) 
    REFERENCES familia 
    ( 
     id_familia
    ) 
    NOT DEFERRABLE 
;


ALTER TABLE producto_pedido 
    ADD CONSTRAINT producto_pedido_pedido_FK FOREIGN KEY 
    ( 
     pedido_id_pedido
    ) 
    REFERENCES pedido 
    ( 
     id_pedido
    ) 
    NOT DEFERRABLE 
;


ALTER TABLE producto_pedido 
    ADD CONSTRAINT producto_pedido_producto_FK FOREIGN KEY 
    ( 
     producto_id_producto
    ) 
    REFERENCES producto 
    ( 
     id_producto
    ) 
    NOT DEFERRABLE 
;


ALTER TABLE producto 
    ADD CONSTRAINT producto_proveedor_FK FOREIGN KEY 
    ( 
     proveedor_id_prov
    ) 
    REFERENCES proveedor 
    ( 
     id_proveedor
    ) 
    NOT DEFERRABLE 
;


ALTER TABLE rubro_proveedor 
    ADD CONSTRAINT rubro_proveedor_proveedor_FKv2 FOREIGN KEY 
    ( 
     proveedor_id_prov
    ) 
    REFERENCES proveedor 
    ( 
     id_proveedor
    ) 
    NOT DEFERRABLE 
;


ALTER TABLE rubro_proveedor 
    ADD CONSTRAINT rubro_proveedor_rubro_FK FOREIGN KEY 
    ( 
     rubro_id_rubro
    ) 
    REFERENCES rubro 
    ( 
     id_rubro
    ) 
    NOT DEFERRABLE 
;


ALTER TABLE usuario 
    ADD CONSTRAINT usuario_persona_FK FOREIGN KEY 
    ( 
     persona_rut
    ) 
    REFERENCES persona 
    ( 
     rut
    ) 
    NOT DEFERRABLE 
;


ALTER TABLE usuario 
    ADD CONSTRAINT usuario_tipo_usuario_FKv2 FOREIGN KEY 
    ( 
     tipo_usuario_tipo
    ) 
    REFERENCES tipo_usuario 
    ( 
     id_tipo
    ) 
    NOT DEFERRABLE 
;



-- Oracle SQL Developer Data Modeler Summary Report: 
-- 
-- CREATE TABLE                            14
-- CREATE INDEX                             0
-- ALTER TABLE                             30
-- CREATE VIEW                              0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE STRUCTURED TYPE                   0
-- CREATE COLLECTION TYPE                   0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
