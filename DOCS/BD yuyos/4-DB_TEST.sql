/*
 *   SCRIPT PARA POBLAR DB
 *   Ingresarlo en orden o todo se ira muy lejos
 */

--INSERT RUBROS 
INSERT INTO rubro (nombre) --1
  VALUES('PERECIBLES');
INSERT INTO rubro (nombre) --2
  VALUES('NO PERECIBLES');
INSERT INTO rubro (nombre) --3
  VALUES('ABARROTES');
INSERT INTO rubro (nombre) --4
  VALUES('ASEO');
INSERT INTO rubro (nombre) --5
  VALUES('PERFUMERIA');


-- INSERT  FAMILIAS
INSERT INTO familia ( nombre, descripcion) --1
  VALUES('LACTEOS', 'PRODUCTOS LACTEOS');
INSERT INTO familia ( nombre, descripcion) --2
  VALUES('FIAMBRES', 'FIAMBRERIA');
INSERT INTO familia ( nombre, descripcion) --3
  VALUES('QUESOS', 'QUESERIA');
INSERT INTO familia ( nombre, descripcion) --4
  VALUES('CONGELADOS', 'PRODUCTOS CONGELADOS');
INSERT INTO familia ( nombre, descripcion) --5
  VALUES('ABARROTES', 'FIDEOS, ARROZ, SAL, AZUCAR, ETC.');
INSERT INTO familia ( nombre, descripcion) --6
  VALUES('CUIDADO PERSONAL', 'ARTICULOS DE ASEO y CUIDADO PERSONAS');
  
  
-- INSERT PROVEEDORES
INSERT INTO proveedor (nombre, rut_empresa, dv_empresa, telefono, direccion, mail) --1
  VALUES('COLUN', '793142685','5', '971346852', 'EL SUREH, PUERTO MONTT', 'contacto@colun.cl'); 
INSERT INTO proveedor (nombre, rut_empresa, dv_empresa, telefono, direccion, mail) --2
  VALUES('CAROZZI', '75963482','K', '985641325', 'CAMINO INTERNACIONAL 5162, VINA DEL MAR', 'ventas@carozzi.cl');
INSERT INTO proveedor (nombre, rut_empresa, dv_empresa, telefono, direccion, mail) --3
  VALUES('LOBOS', '86452913','2', '975684231', 'ERRAZURIZ 1425, VALPARAISO', 'gestion@sallobos.cl');
INSERT INTO proveedor (nombre, rut_empresa, dv_empresa, telefono, direccion, mail) --4
  VALUES('NIVEA', '1023956841','3', '945683296', 'AV. VICUNA MACKENA 6953, SANTIAGO', 'soporte@nivea.cl');


-- INSERT PRODUCTOS  
INSERT INTO producto (cod_producto, nombre, precio_venta, precio_compra, stock, proveedor_id_prov, familia_id_familia) --1
  VALUES(123654,'LECHE ENTERA 1LT', '650', '500', '15', 1, 1);
INSERT INTO producto (cod_producto, nombre, precio_venta, precio_compra, stock, proveedor_id_prov, familia_id_familia) --2
  VALUES(236925,'LECHE DESCREMADA 1LT', '660', '510', '15', 1, 1);
INSERT INTO producto (cod_producto, nombre, precio_venta, precio_compra, stock, proveedor_id_prov, familia_id_familia) --3
  VALUES(365895,'YOGURT VARIEDADES 130ML', '150', '90', '15', 1, 1);
  
INSERT INTO producto (cod_producto, nombre, precio_venta, precio_compra, stock, proveedor_id_prov, familia_id_familia) --4
  VALUES(365986,'FIDEOS CORBATA 500GR', '590', '460', '20', 2, 5);
INSERT INTO producto (cod_producto, nombre, precio_venta, precio_compra, stock, proveedor_id_prov, familia_id_familia) --5
  VALUES(102369,'TALLARIN N5 350GR', '580', '450', '25', 2, 5);
INSERT INTO producto (cod_producto, nombre, precio_venta, precio_compra, stock, proveedor_id_prov, familia_id_familia) --6
  VALUES(425639,'CAROQUESOS 500GR', '990', '750', '10', 2, 5);
  
INSERT INTO producto (cod_producto, nombre, precio_venta, precio_compra, stock, proveedor_id_prov, familia_id_familia) --7
  VALUES(13698745,'SAL DE MAR 250GR', '500', '400', '15', 3, 5);
INSERT INTO producto (cod_producto, nombre, precio_venta, precio_compra, stock, proveedor_id_prov, familia_id_familia) --8
  VALUES(658952,'SAL DE MESA 1GR', '600', '500', '35', 3, 5);
INSERT INTO producto (cod_producto, nombre, precio_venta, precio_compra, stock, proveedor_id_prov, familia_id_familia) --9
  VALUES(3269554,'BIOSAL 350GR', '650', '500', '10', 3, 5);
  
INSERT INTO producto (cod_producto, nombre, precio_venta, precio_compra, stock, proveedor_id_prov, familia_id_familia) --10
  VALUES(69854785,'DESODORANTE CARE+', '1690', '1300', '5', 4, 6);
INSERT INTO producto (cod_producto, nombre, precio_venta, precio_compra, stock, proveedor_id_prov, familia_id_familia) --11
  VALUES(1369582,'DESODORANTE FOR MEN', '1750', '1400', '5', 4, 6);
INSERT INTO producto (cod_producto, nombre, precio_venta, precio_compra, stock, proveedor_id_prov, familia_id_familia) --12
  VALUES(96587412,'CREMA DE MANOS 150ML', '790', '550', '10', 4, 6);
 
 
-- INSERT PERSONA 
INSERT INTO PERSONA (rut, dv, nombre, paterno, materno, direccion, fono, mail)
  VALUES('7895689','1','Juanita', 'Rosales', 'Mendez', 'PSJ. HOLA 156 VILLA ALEMANA', '958642364', 'yuyitos@mail.cl');
INSERT INTO PERSONA (rut, dv, nombre, paterno, materno, direccion, fono, mail)
  VALUES('15963741','K','ALEJANDRA', 'PEREZ', 'RODRIGUEZ', 'EL PALO 516 QUILPUE', '956842365', 'al_periuez@mail.cl');
INSERT INTO PERSONA (rut, dv, nombre, paterno, materno, direccion, fono, mail)
  VALUES('16495836','2','ALEXANDER', 'RAMIREZ', 'ROWLAND', 'AV. VALAPARISO 362 VILLA ALEMANA', '985476321', 'ramirez.alex@mail.com');
INSERT INTO PERSONA (rut, dv, nombre, paterno, materno, direccion, fono, mail)
  VALUES('18596742','6','ROSALIA', 'SOZA', 'FLORES', 'LAS AMERICAS 1987', '942589657', 'resalia@mail.cl');
  
  
-- INSERT TIPO USUARIOS
INSERT INTO tipo_usuario (nombre_tipo) --1
  VALUES('ADMIN');
INSERT INTO tipo_usuario (nombre_tipo) --2
  VALUES('VENDEDOR');
INSERT INTO tipo_usuario (nombre_tipo) --3
  VALUES('CLIENTE');


-- INSERT USUARIOS
INSERT INTO usuario (clave, tipo_usuario_tipo, persona_rut) --1
  VALUES ('yuyos', 1, 7895689);
INSERT INTO usuario (clave, tipo_usuario_tipo, persona_rut) --2
  VALUES ('yuyos', 2, 7895689);
 
  
-- INSERT CLIENTES
 INSERT INTO clientes (estado_deuda, fiado, persona_rut) --1
  VALUES('P',0,16495836);
 INSERT INTO clientes (estado_deuda, fiado, persona_rut) --2
  VALUES('F',0,18596742);
  
  
-- INSERT BOLETAS  
INSERT INTO boletas (fecha, total, estado, usuario_id_usuario) --1
  VALUES(TO_DATE('2019/03/31 12:00:00', 'yyyy/mm/dd hh:mi:ss'),'15900','P',2);
INSERT INTO boletas (fecha, total, estado, usuario_id_usuario, clientes_id_cliente) --2
  VALUES(TO_DATE('2019/04/01 09:34:41', 'yyyy/mm/dd hh:mi:ss'),'3100','F',1, 2);
  
  
-- INSERT PRODUCTO_BOLETA
INSERT INTO producto_boleta (boletas_id_boletas, producto_id_producto, cantidad, descuento, valor)
  VALUES(1, 1, 3, 0, 1950);
INSERT INTO producto_boleta (boletas_id_boletas, producto_id_producto, cantidad, descuento, valor)
  VALUES(1, 2, 2, 0, 1320);
INSERT INTO producto_boleta (boletas_id_boletas, producto_id_producto, cantidad, descuento, valor)
  VALUES(1, 3, 1, 0, 130);
  
INSERT INTO producto_boleta (boletas_id_boletas, producto_id_producto, cantidad, descuento, valor)
  VALUES(2, 11, 1, 0, 1790);
INSERT INTO producto_boleta (boletas_id_boletas, producto_id_producto, cantidad, descuento, valor)
  VALUES(2, 2, 1, 0, 660);
INSERT INTO producto_boleta (boletas_id_boletas, producto_id_producto, cantidad, descuento, valor)
  VALUES(2, 9, 1, 0, 650);
 
  
-- INSERT DEUDA
INSERT INTO deuda (boletas_id_boletas, clientes_id_cliente, monto, estado)
  VALUES(2, 2, 3100, 'V');
  

-- INSERT RUBRO_PROVEEDOR
INSERT INTO rubro_proveedor(proveedor_id_prov,rubro_id_rubro)
  VALUES(1, 1);


-- INSERT PEDIDO
INSERT INTO pedido(valor,fecha_pedido,fecha_estimada)
  VALUES('49150', CURRENT_DATE, CURRENT_DATE + 5);


-- INSERT PRODUCTO_PEDIDO
INSERT INTO producto_pedido (pedido_id_pedido, producto_id_producto, precio_unitario, cantidad)
  VALUES(1, 1, 500, 10);
INSERT INTO producto_pedido (pedido_id_pedido, producto_id_producto, precio_unitario, cantidad)
  VALUES(1, 3, 90, 10);
INSERT INTO producto_pedido (pedido_id_pedido, producto_id_producto, precio_unitario, cantidad)
  VALUES(1, 10, 1300, 15);
INSERT INTO producto_pedido (pedido_id_pedido, producto_id_producto, precio_unitario, cantidad)
  VALUES(1, 11, 1400, 15);
INSERT INTO producto_pedido (pedido_id_pedido, producto_id_producto, precio_unitario, cantidad)
  VALUES(1, 12, 550, 5);
  