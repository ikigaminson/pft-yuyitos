-- Generado por Oracle SQL Developer Data Modeler 4.1.3.901
--   en:        2019-03-27 00:13:51 CLST
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g




CREATE TABLE boletas
  (
    id_boletas           VARCHAR2 (20) NOT NULL ,
    fecha                DATE NOT NULL ,
    total                VARCHAR2 (20) NOT NULL ,
    producto_id_producto VARCHAR2 (20) NOT NULL ,
    clientes_id_cliente  VARCHAR2 (20) ,
    usuario_id_usuario   VARCHAR2 (15) NOT NULL ,
    cantidad             VARCHAR2 (20) NOT NULL ,
    id_cliente           VARCHAR2 (20)
  ) ;
ALTER TABLE boletas ADD CONSTRAINT boletas_PK PRIMARY KEY ( id_boletas ) ;


CREATE TABLE clientes
  (
    id_cliente     VARCHAR2 (20) NOT NULL ,
    nombre         VARCHAR2 (25) NOT NULL ,
    apellido       VARCHAR2 (25 CHAR) NOT NULL ,
    telefono       INTEGER NOT NULL ,
    deuda          INTEGER NOT NULL ,
    direccion      VARCHAR2 (30) NOT NULL ,
    deuda_id_deuda VARCHAR2 (20) NOT NULL ,
    id_deuda       VARCHAR2 (20) NOT NULL
  ) ;
CREATE UNIQUE INDEX clientes__idx ON clientes
  (
    deuda_id_deuda ASC
  )
  ;
ALTER TABLE clientes ADD CONSTRAINT clientes_PK PRIMARY KEY ( id_cliente ) ;


CREATE TABLE deuda
  (
    id_deuda   VARCHAR2 (20) NOT NULL ,
    monto      VARCHAR2 (20) NOT NULL ,
    estado     VARCHAR2 (15) NOT NULL ,
    fecha_pago DATE NOT NULL ,
    id_cliente VARCHAR2 (20)
  ) ;
ALTER TABLE deuda ADD CONSTRAINT deuda_PK PRIMARY KEY ( id_deuda ) ;


CREATE TABLE familia
  (
    id_familia  VARCHAR2 (20) NOT NULL ,
    nombre      VARCHAR2 (20) NOT NULL ,
    descripcion VARCHAR2 (50)
  ) ;
ALTER TABLE familia ADD CONSTRAINT familia_PK PRIMARY KEY ( id_familia ) ;


CREATE TABLE producto
  (
    id_producto        VARCHAR2 (20) NOT NULL ,
    nombre             VARCHAR2 (25) NOT NULL ,
    precio             VARCHAR2 (20) NOT NULL ,
    stock              VARCHAR2 (15) NOT NULL ,
    proveedor_id_prov  INTEGER NOT NULL ,
    familia_id_familia VARCHAR2 (20) NOT NULL
  ) ;
ALTER TABLE producto ADD CONSTRAINT producto_PK PRIMARY KEY ( id_producto ) ;


CREATE TABLE proveedor
  (
    id_prov     INTEGER NOT NULL ,
    nombre      VARCHAR2 (25) NOT NULL ,
    rut_empresa VARCHAR2 (25) NOT NULL ,
    telefono    VARCHAR2 (20) NOT NULL ,
    direccion   VARCHAR2 (20) NOT NULL ,
    mail        VARCHAR2 (25)
  ) ;
ALTER TABLE proveedor ADD CONSTRAINT proveedor_PK PRIMARY KEY ( id_prov ) ;


CREATE TABLE registro_pedido
  (
    id_pedido            VARCHAR2 (25 CHAR) NOT NULL ,
    cantidad             VARCHAR2 (15) NOT NULL ,
    fecha_pedido         DATE NOT NULL ,
    fecha_llegada        DATE NOT NULL ,
    producto_id_producto VARCHAR2 (20) NOT NULL
  ) ;
ALTER TABLE registro_pedido ADD CONSTRAINT registro_pedido_PK PRIMARY KEY ( id_pedido, producto_id_producto ) ;


CREATE TABLE rubro
  (
    id_rubro VARCHAR2 (15) NOT NULL ,
    nombre   VARCHAR2 (25) NOT NULL
  ) ;
ALTER TABLE rubro ADD CONSTRAINT rubro_PK PRIMARY KEY ( id_rubro ) ;


CREATE TABLE rubro_proveedor
  (
    rubro_id_rubro    VARCHAR2 (15) NOT NULL ,
    proveedor_id_prov INTEGER NOT NULL ,
    id_prov           INTEGER NOT NULL
  ) ;


CREATE TABLE tipo_usuario
  ( tipo VARCHAR2 (15) NOT NULL
  ) ;
ALTER TABLE tipo_usuario ADD CONSTRAINT tipo_usuario_PK PRIMARY KEY ( tipo ) ;


CREATE TABLE usuario
  (
    id_usuario        VARCHAR2 (15) NOT NULL ,
    nombre            VARCHAR2 (20) NOT NULL ,
    clave             VARCHAR2 (15) NOT NULL ,
    tipo_usuario_tipo VARCHAR2 (15) NOT NULL
  ) ;
ALTER TABLE usuario ADD CONSTRAINT usuario_PK PRIMARY KEY ( id_usuario ) ;


ALTER TABLE boletas ADD CONSTRAINT boletas_clientes_FKv2 FOREIGN KEY ( clientes_id_cliente ) REFERENCES clientes ( id_cliente ) NOT DEFERRABLE ;

ALTER TABLE boletas ADD CONSTRAINT boletas_producto_FK FOREIGN KEY ( producto_id_producto ) REFERENCES producto ( id_producto ) NOT DEFERRABLE ;

ALTER TABLE boletas ADD CONSTRAINT boletas_usuario_FK FOREIGN KEY ( usuario_id_usuario ) REFERENCES usuario ( id_usuario ) NOT DEFERRABLE ;

ALTER TABLE clientes ADD CONSTRAINT clientes_deuda_FKv2 FOREIGN KEY ( deuda_id_deuda ) REFERENCES deuda ( id_deuda ) NOT DEFERRABLE ;

ALTER TABLE producto ADD CONSTRAINT producto_familia_FK FOREIGN KEY ( familia_id_familia ) REFERENCES familia ( id_familia ) NOT DEFERRABLE ;

ALTER TABLE producto ADD CONSTRAINT producto_proveedor_FK FOREIGN KEY ( proveedor_id_prov ) REFERENCES proveedor ( id_prov ) NOT DEFERRABLE ;

ALTER TABLE registro_pedido ADD CONSTRAINT registro_pedido_producto_FK FOREIGN KEY ( producto_id_producto ) REFERENCES producto ( id_producto ) NOT DEFERRABLE ;

ALTER TABLE rubro_proveedor ADD CONSTRAINT rubro_proveedor_proveedor_FKv2 FOREIGN KEY ( proveedor_id_prov ) REFERENCES proveedor ( id_prov ) NOT DEFERRABLE ;

ALTER TABLE rubro_proveedor ADD CONSTRAINT rubro_proveedor_rubro_FK FOREIGN KEY ( rubro_id_rubro ) REFERENCES rubro ( id_rubro ) NOT DEFERRABLE ;

ALTER TABLE usuario ADD CONSTRAINT usuario_tipo_usuario_FKv2 FOREIGN KEY ( tipo_usuario_tipo ) REFERENCES tipo_usuario ( tipo ) NOT DEFERRABLE ;


-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                            11
-- CREATE INDEX                             1
-- ALTER TABLE                             20
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
