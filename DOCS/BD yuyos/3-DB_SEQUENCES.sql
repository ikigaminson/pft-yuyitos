/*
 *  SCRIPT PARA GENERAR AUTO INCREMENT
 *  Se crea una secuencia y luego se crea un trigger que se ejecuta
 *  cada vez que se hace una insercion
 *
 */

-- BOLETAS SEQ
CREATE SEQUENCE boletas_seq START WITH 1;

CREATE OR REPLACE TRIGGER boletas_bi
BEFORE INSERT ON boletas
FOR EACH ROW
BEGIN
  SELECT boletas_seq.nextval
  INTO :new.id_boletas
  FROM dual;
END;
/

-- CLIENTES SEQ
CREATE SEQUENCE clientes_seq START WITH 1;

CREATE OR REPLACE TRIGGER clientes_bi
BEFORE INSERT ON clientes
FOR EACH ROW
BEGIN
  SELECT clientes_seq.nextval
  INTO :new.id_cliente
  FROM dual;
END;
/

-- DEUDA SEQ
CREATE SEQUENCE deuda_seq START WITH 1;

CREATE OR REPLACE TRIGGER deuda_bi
BEFORE INSERT ON deuda
FOR EACH ROW
BEGIN
  SELECT deuda_seq.nextval
  INTO :new.id_deuda
  FROM dual;
END;
/

-- FAMILIA SEQ
CREATE SEQUENCE familia_seq START WITH 1;

CREATE OR REPLACE TRIGGER familia_bi
BEFORE INSERT ON familia
FOR EACH ROW
BEGIN
  SELECT familia_seq.nextval
  INTO :new.id_familia
  FROM dual;
END;
/

-- PEDIDO SEQ
CREATE SEQUENCE pedido_seq START WITH 1;

CREATE OR REPLACE TRIGGER pedido_bi
BEFORE INSERT ON pedido
FOR EACH ROW
BEGIN
  SELECT pedido_seq.nextval
  INTO :new.id_pedido
  FROM dual;
END;
/

-- PRODUCTO SEQ
CREATE SEQUENCE producto_seq START WITH 1;

CREATE OR REPLACE TRIGGER producto_bi
BEFORE INSERT ON producto
FOR EACH ROW
BEGIN
  SELECT producto_seq.nextval
  INTO :new.id_producto
  FROM dual;
END;
/

-- PRODUCTO_BOLETA SEQ
CREATE SEQUENCE producto_boleta_seq START WITH 1;

CREATE OR REPLACE TRIGGER producto_boleta_bi
BEFORE INSERT ON producto_boleta
FOR EACH ROW
BEGIN
  SELECT producto_boleta_seq.nextval
  INTO :new.id_row
  FROM dual;
END;
/

-- PROVEEDOR SEQ
CREATE SEQUENCE proveedor_seq START WITH 1;

CREATE OR REPLACE TRIGGER proveedor_bi
BEFORE INSERT ON proveedor
FOR EACH ROW
BEGIN
  SELECT proveedor_seq.nextval
  INTO :new.id_proveedor
  FROM dual;
END;
/

-- RUBRO SEQ
CREATE SEQUENCE rubro_seq START WITH 1;

CREATE OR REPLACE TRIGGER rubro_bi
BEFORE INSERT ON rubro
FOR EACH ROW
BEGIN
  SELECT rubro_seq.nextval
  INTO :new.id_rubro
  FROM dual;
END;
/

-- RUBRBO_PROVEEDOR SEQ
CREATE SEQUENCE rubro_proveedor_seq START WITH 1;

CREATE OR REPLACE TRIGGER rubro_proveedor_bi
BEFORE INSERT ON rubro_proveedor
FOR EACH ROW
BEGIN
  SELECT rubro_proveedor_seq.nextval
  INTO :new.id_row
  FROM dual;
END;
/

-- TIPO_USUARIOSEQ
CREATE SEQUENCE tipo_usuario_seq START WITH 1;

CREATE OR REPLACE TRIGGER tipo_usuario_bi
BEFORE INSERT ON tipo_usuario
FOR EACH ROW
BEGIN
  SELECT tipo_usuario_seq.nextval
  INTO :new.id_tipo
  FROM dual;
END;
/

-- USUARIO SEQ
CREATE SEQUENCE usuario_seq START WITH 1;

CREATE OR REPLACE TRIGGER usuario_bi
BEFORE INSERT ON usuario
FOR EACH ROW
BEGIN
  SELECT usuario_seq.nextval
  INTO :new.id_usuario
  FROM dual;
END;
/

