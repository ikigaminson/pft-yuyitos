/*
 *  SCRIPT CREACION DB
 *  Ejecutar como SYSADM y despues crea una nueva conexion
 *  con usuario YUYITOS_ADMIN en ORCL
 *
 */

CREATE USER yuyitos_admin IDENTIFIED BY pass123;

GRANT CONNECT TO yuyitos_admin;

GRANT CONNECT, RESOURCE, DBA TO yuyitos_admin;

GRANT ALL PRIVILEGES TO yuyitos_admin;

GRANT UNLIMITED TABLESPACE TO yuyitos_admin;

/*  No ejecutar por ahora
GRANT SELECT, INSERT, UPDATE, DELETE TO yuyitos_admin;
*/