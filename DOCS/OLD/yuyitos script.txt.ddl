-- Generado por Oracle SQL Developer Data Modeler 18.3.0.268.1156
--   en:        2019-03-25 16:03:03 CLST
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g



CREATE TABLE boletas (
    id_boletas             VARCHAR2(20) NOT NULL,
    fecha                  DATE NOT NULL,
    total                  VARCHAR2(20) NOT NULL,
    producto_id_producto   VARCHAR2(20) NOT NULL,
    clientes_id_cliente    VARCHAR2(20),
    usuario_id_usuario     VARCHAR2(20) NOT NULL,
    cantidad               VARCHAR2(20) NOT NULL
);

ALTER TABLE boletas ADD CONSTRAINT boletas_pk PRIMARY KEY ( id_boletas );

CREATE TABLE clientes (
    id_cliente       VARCHAR2(20) NOT NULL,
    nombre           VARCHAR2(25) NOT NULL,
    apellido         VARCHAR2(25 CHAR) NOT NULL,
    telefono         INTEGER NOT NULL,
    deuda            INTEGER NOT NULL,
    direccion        VARCHAR2(30) NOT NULL,
    deuda_id_deuda   VARCHAR2(20) NOT NULL
);

CREATE UNIQUE INDEX clientes__idx ON
    clientes (
        deuda_id_deuda
    ASC );

ALTER TABLE clientes ADD CONSTRAINT clientes_pk PRIMARY KEY ( id_cliente );

CREATE TABLE deuda (
    id_deuda     VARCHAR2(20) NOT NULL,
    monto        VARCHAR2(20) NOT NULL,
    estado       VARCHAR2(15) NOT NULL,
    fecha_pago   DATE NOT NULL,
    id_cliente   VARCHAR2(20)
);

ALTER TABLE deuda ADD CONSTRAINT deuda_pk PRIMARY KEY ( id_deuda );

CREATE TABLE familia (
    id_familia   VARCHAR2(20) NOT NULL,
    nombre       VARCHAR2(20) NOT NULL
);

ALTER TABLE familia ADD CONSTRAINT familia_pk PRIMARY KEY ( id_familia );

CREATE TABLE producto (
    id_producto          VARCHAR2(20) NOT NULL,
    nombre               VARCHAR2(25) NOT NULL,
    precio               VARCHAR2(20) NOT NULL,
    stock                VARCHAR2(15) NOT NULL,
    proveedor_id_prov    INTEGER NOT NULL,
    familia_id_familia   VARCHAR2(20) NOT NULL
);

ALTER TABLE producto ADD CONSTRAINT producto_pk PRIMARY KEY ( id_producto );

CREATE TABLE proveedor (
    id_prov       INTEGER NOT NULL,
    nombre        VARCHAR2(25) NOT NULL,
    rut_empresa   VARCHAR2(25) NOT NULL,
    telefono      VARCHAR2(20) NOT NULL,
    direccion     VARCHAR2(20) NOT NULL,
    mail          VARCHAR2(25)
);

ALTER TABLE proveedor ADD CONSTRAINT proveedor_pk PRIMARY KEY ( id_prov );

CREATE TABLE registro_pedido (
    id_pedido              VARCHAR2(25 CHAR) NOT NULL,
    cantidad               VARCHAR2(15) NOT NULL,
    fecha_pedido           DATE NOT NULL,
    fecha_llegada          DATE NOT NULL,
    producto_id_producto   VARCHAR2(20) NOT NULL
);

ALTER TABLE registro_pedido ADD CONSTRAINT registro_pedido_pk PRIMARY KEY ( id_pedido,
                                                                            producto_id_producto );

CREATE TABLE rubro (
    id_rubro   VARCHAR2(15) NOT NULL,
    nombre     VARCHAR2(25) NOT NULL
);

ALTER TABLE rubro ADD CONSTRAINT rubro_pk PRIMARY KEY ( id_rubro );

CREATE TABLE rubro_proveedor (
    rubro_id_rubro      VARCHAR2(15) NOT NULL,
    proveedor_id_prov   INTEGER NOT NULL
);

CREATE TABLE tipo_usuario (
    tipo   VARCHAR2(15) NOT NULL
);

ALTER TABLE tipo_usuario ADD CONSTRAINT tipo_usuario_pk PRIMARY KEY ( tipo );

CREATE TABLE usuario (
    id_usuario          VARCHAR2(15) NOT NULL,
    nombre              VARCHAR2(20) NOT NULL,
    clave               VARCHAR2(15) NOT NULL,
    tipo_usuario_tipo   VARCHAR2(15) NOT NULL
);

ALTER TABLE usuario ADD CONSTRAINT usuario_pk PRIMARY KEY ( id_usuario );

ALTER TABLE boletas
    ADD CONSTRAINT boletas_clientes_fk FOREIGN KEY ( clientes_id_cliente )
        REFERENCES clientes ( id_cliente );

ALTER TABLE boletas
    ADD CONSTRAINT boletas_producto_fk FOREIGN KEY ( producto_id_producto )
        REFERENCES producto ( id_producto );

ALTER TABLE boletas
    ADD CONSTRAINT boletas_usuario_fk FOREIGN KEY ( usuario_id_usuario )
        REFERENCES usuario ( id_usuario );

ALTER TABLE clientes
    ADD CONSTRAINT clientes_deuda_fk FOREIGN KEY ( deuda_id_deuda )
        REFERENCES deuda ( id_deuda );

ALTER TABLE deuda
    ADD CONSTRAINT deuda_clientes_fk FOREIGN KEY ( id_cliente )
        REFERENCES clientes ( id_cliente );

ALTER TABLE producto
    ADD CONSTRAINT producto_familia_fk FOREIGN KEY ( familia_id_familia )
        REFERENCES familia ( id_familia );

ALTER TABLE producto
    ADD CONSTRAINT producto_proveedor_fk FOREIGN KEY ( proveedor_id_prov )
        REFERENCES proveedor ( id_prov );

ALTER TABLE registro_pedido
    ADD CONSTRAINT registro_pedido_producto_fk FOREIGN KEY ( producto_id_producto )
        REFERENCES producto ( id_producto );

ALTER TABLE rubro_proveedor
    ADD CONSTRAINT rubro_proveedor_proveedor_fk FOREIGN KEY ( proveedor_id_prov )
        REFERENCES proveedor ( id_prov );

ALTER TABLE rubro_proveedor
    ADD CONSTRAINT rubro_proveedor_rubro_fk FOREIGN KEY ( rubro_id_rubro )
        REFERENCES rubro ( id_rubro );

ALTER TABLE usuario
    ADD CONSTRAINT usuario_tipo_usuario_fk FOREIGN KEY ( tipo_usuario_tipo )
        REFERENCES tipo_usuario ( tipo );

ALTER TABLE boletas
    ADD CONSTRAINT boletas_clientes_fk FOREIGN KEY ( clientes_id_cliente )
        REFERENCES clientes ( id_cliente );

ALTER TABLE boletas
    ADD CONSTRAINT boletas_producto_fk FOREIGN KEY ( producto_id_producto )
        REFERENCES producto ( id_producto );

ALTER TABLE boletas
    ADD CONSTRAINT boletas_usuario_fk FOREIGN KEY ( usuario_id_usuario )
        REFERENCES usuario ( id_usuario );

ALTER TABLE clientes
    ADD CONSTRAINT clientes_deuda_fk FOREIGN KEY ( deuda_id_deuda )
        REFERENCES deuda ( id_deuda );

ALTER TABLE deuda
    ADD CONSTRAINT deuda_clientes_fk FOREIGN KEY ( id_cliente )
        REFERENCES clientes ( id_cliente );

ALTER TABLE producto
    ADD CONSTRAINT producto_familia_fk FOREIGN KEY ( familia_id_familia )
        REFERENCES familia ( id_familia );

ALTER TABLE producto
    ADD CONSTRAINT producto_proveedor_fk FOREIGN KEY ( proveedor_id_prov )
        REFERENCES proveedor ( id_prov );

ALTER TABLE registro_pedido
    ADD CONSTRAINT registro_pedido_producto_fk FOREIGN KEY ( producto_id_producto )
        REFERENCES producto ( id_producto );

ALTER TABLE rubro_proveedor
    ADD CONSTRAINT rubro_proveedor_proveedor_fk FOREIGN KEY ( proveedor_id_prov )
        REFERENCES proveedor ( id_prov );

ALTER TABLE rubro_proveedor
    ADD CONSTRAINT rubro_proveedor_rubro_fk FOREIGN KEY ( rubro_id_rubro )
        REFERENCES rubro ( id_rubro );

ALTER TABLE usuario
    ADD CONSTRAINT usuario_tipo_usuario_fk FOREIGN KEY ( tipo_usuario_tipo )
        REFERENCES tipo_usuario ( tipo );



-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                            11
-- CREATE INDEX                             1
-- ALTER TABLE                             32
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE MATERIALIZED VIEW LOG             0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
