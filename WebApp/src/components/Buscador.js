 import React from 'react';
import {Input,Form} from 'reactstrap';

function Buscador() {

 
    return (
 
 <Form className="form-inline my-2 my-lg-0">
      <Input className="form-control mr-sm-2" type="text" placeholder="Buscar"/>
      <button className="btn btn-secondary my-2 my-sm-0" type="submit">Buscar</button>
   </Form>

    )

}

export default Buscador;