import React from 'react';
import { Link, Redirect} from "react-router-dom";
import {Collapse,NavbarToggler} from 'reactstrap';
import logo from '../logo.png';

class HeaderVendedor extends React.Component {
constructor(props) {
  super(props);

  const tokenIn = localStorage.getItem("acept")
  let loggedIn = true 

  if (tokenIn == null) {
    loggedIn = false
  }

  this.state = {
    loggedOut:false,
    isOpen: false,
    loggedIn
  }
this.salir = this.salir.bind(this);
this.toggle = this.toggle.bind(this);
}

toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
salir(){
this.setState({loggedOut:true})
localStorage.removeItem("acept")
localStorage.removeItem("ventas")
}


 render(){
  
 var styles = {
  color:'red',
  background: 'rgb(44,109,193)',
  background: 'linear-gradient(0deg, rgba(44,109,193,1) 0%, rgba(21,32,51,1) 100%)'
  }
   if (!this.state.loggedIn||this.state.loggedOut) {
    return <Redirect to="/Login"/>
   }
 
    return (

      <div>
  <nav className="navbar navbar-expand-lg navbar-dark " style={styles}>

<Link className="navbar-brand" to="/"><img src={logo}  height="80" width="80" background= "transparent"  /></Link>


  <NavbarToggler onClick={this.toggle} />
  <Collapse isOpen={this.state.isOpen} navbar>

    <ul className="navbar-nav mr-auto" navbar>

      <li className="nav-item active">
      <Link className="nav-link" ><h4>Inicio</h4></Link>
      </li>

      <li className="nav-item active">
        <Link className="nav-link" ><h4>Clientes</h4></Link>
      </li>

      <li className="nav-item active">
        <Link className="nav-link" ><h4>Proveedores</h4></Link>
      </li>

      <li className="nav-item active">
       <Link className="nav-link" to="/Ventas"><h4>Ventas</h4></Link>
      </li>

       <li className="nav-item active">
       <Link className="nav-link" ><h4>Pedidos</h4></Link>
      </li>

       <li className="nav-item active">
       <Link className="nav-link" ><h4>Productos</h4></Link>
      </li>

    </ul>
    <form className="form-inline my-2 my-lg-0">
    <button type="button" onClick={this.salir} className="btn btn-success"><h5>Salir</h5></button>
    </form>

  </Collapse>
</nav>
      </div>
    );
  }
}

export default HeaderVendedor;