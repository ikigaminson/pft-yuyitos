import React from 'react';
import { Link, Redirect} from "react-router-dom";
import {Collapse,NavbarToggler} from 'reactstrap';
import logo from '../logo.png';

class HeaderAdmin extends React.Component {
constructor(props) {
  super(props);

  const tokenIn = localStorage.getItem("acept")
  const token = localStorage.getItem("ventas")
  let loggedIn = true 
  let ventasIn = true

  if (tokenIn === null) {
    loggedIn = false 
  }
  if (token === null) {
    ventasIn = false
  }
  this.state = {
    loggedOut:false,
    isOpen: false,
    loggedIn,
    ventasIn
  }
this.salir = this.salir.bind(this);
this.toggle = this.toggle.bind(this);
}

toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
salir(){
this.setState({loggedOut:true})
localStorage.removeItem("acept")
localStorage.removeItem("ventas")
}


 render(){
  
  var styles = {
  color:'red',
  background: 'rgb(44,109,193)',
  background: 'linear-gradient(0deg, rgba(44,109,193,1) 0%, rgba(21,32,51,1) 100%)'
  }
   if (!this.state.loggedIn||this.state.loggedOut) {
    return <Redirect to="/Login"/>
   }
   if (this.state.ventasIn) {
    return <Redirect to="/Ventas"/>
   }
 
    return (

      <div>
  <nav className="navbar navbar-expand-lg navbar-dark " style={styles}>

  <Link className="navbar-brand" to="/"><img src={logo}  height="80" width="80" background= "transparent"  /></Link>


  <NavbarToggler onClick={this.toggle} />
  <Collapse isOpen={this.state.isOpen} navbar>

    <ul className="navbar-nav mr-auto" navbar>

      <li className="nav-item active">
      <Link className="nav-link" to="/"><h4>Inicio</h4></Link>
      </li>

      <li className="nav-item active">
        <Link className="nav-link" to="/Clientes"><h4>Clientes</h4></Link>
      </li>

      <li className="nav-item active">
        <Link className="nav-link" to="/Proveedores"><h4>Proveedores</h4></Link>
      </li>

      <li className="nav-item active">
       <Link className="nav-link" to="/Ventas"><h4>Ventas</h4></Link>
      </li>

       <li className="nav-item active">
       <Link className="nav-link" to="/Pedidos"><h4>Pedidos</h4></Link>
      </li>

       <li className="nav-item active">
       <Link className="nav-link" to="/Productos"><h4>Productos</h4></Link>
      </li>

    </ul>
    <form className="form-inline my-2 my-lg-0">
    <button type="button" onClick={this.salir} className="btn btn-success"><h5>Salir</h5></button>
    </form>

  </Collapse>
</nav>
      </div>
    );
  }
}

export default HeaderAdmin;