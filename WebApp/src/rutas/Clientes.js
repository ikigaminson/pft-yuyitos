import React from 'react';
import axios from 'axios';
import { CardDeck , Card, CardHeader,CardBody, Row,Col,Container} from 'reactstrap';
import { Button, Label, Form, FormGroup,Input,Table,Alert} from 'reactstrap';
import HeaderAdmin from '../components/HeaderAdmin';
import TituloPagina from '../components/TituloPagina';
import Buscador from '../components/Buscador';
import Footer from '../components/Footer';

class Clientes extends React.Component{

  state = {
    url:'http://54.186.20.165:4567/clientes',

    clientes:[],

    dataClientes:{
      id: '',
      rut:'',
      dv:'',
      nombre:'',
      paterno:'',
      materno:'',
      fono:'',
      direccion:'',
      mail:'',
    },

    mensage:{
      texto:'',
      visible:false
    }
  }

  componentWillMount() {
    this._refreshBooks();
  }


  ingresar() {
    axios.post(this.state.url+'/insert', this.state.dataClientes)

    .then((response) => {
      let { clientes } = this.state;
      this._refreshBooks();

      this.setState({ clientes, dataClientes: {
        rut:'',
        dv:'',
        nombre:'',
        paterno:'',
        materno:'',
        fono:'',
        direccion:'',
        mail:'',
      }});
    
    if (this.state.dataClientes.rut === '') {
       this.setState({
      mensage:{
      texto:'Cliente Agregado',
        visible:true}});
    }else{
      this.setState({
      mensage:{
      texto:'Cliente Agregado',
        visible:true}});
    }
  })}

  actualizar() {
    let { rut, dv, nombre, paterno, materno, fono, direccion, mail } = this.state.dataClientes;

    axios.post(this.state.url+'/update/' + this.state.dataClientes.id, {
      rut, dv, nombre, paterno, materno, fono, direccion, mail
    })
    .then((response) => {
      this._refreshBooks();
      this.setState({ dataClientes: {
        id:'',
        rut:'',
        dv:'',
        nombre:'',
        paterno:'',
        materno:'',
        fono:'',
        direccion:'',
        mail:'',

      },mensage:{
        texto:'Cliente Actualizado',
        visible:true}});
    })
    .catch(e => console.log(e));
  }

  cargarMod(id, rut, dv, nombre, paterno, materno, fono, direccion, mail) {
    this.setState({dataClientes: { id, rut, dv, nombre, paterno, materno, fono, direccion, mail }}) 
  }



  eliminar(id) {
    axios.post(this.state.url+'/delete/'+ id)
    .then((response) => {
      this._refreshBooks();
      this.setState({mensage:{
      texto:'Cliente Eliminado',
      visible:true}});
    })
    .catch(e => console.log(e)); 
  }

_refreshBooks() {
   axios.get(this.state.url+'/all')
   .then((response) => {this.setState({clientes: response.data})})
   .catch(e => console.log(e));
}

 render(){
   let clientes = this.state.clientes.map((cliente) => {
    return (
      <tr key={cliente.id}>

      <td>{cliente.persona.nombre}</td> 
      <td>{cliente.persona.paterno} {cliente.persona.materno}</td>
      <td>{cliente.persona.fono}</td>
      <td>{cliente.estadoDeuda}</td>
      <td>{cliente.fiado}</td>
      <td>{cliente.persona.direccion}</td>
      <td>
      <Button color="danger" size="sm" 
      onClick={this.eliminar.bind(this, cliente.id)}
      >Eliminar</Button>
      </td>

      <td>
      <Button color="success" size="sm" className="mr-2" 
      onClick={this.cargarMod.bind
      (this, cliente.id,cliente.persona.rut,cliente.persona.dv,cliente.persona.nombre,cliente.persona.paterno,cliente.persona.materno,
        cliente.persona.fono,cliente.persona.direccion,cliente.persona.mail)}
      >Modificar</Button>
      </td>
      </tr>
      )
    });

    return (
    <div>

    <HeaderAdmin/>

    <Container fluid >
    <div>
    
    <TituloPagina titulo="CLIENTES"/>

    <Row> 
    <Col sm={{ size: '4', offset: 0 }}>

    <Card body outline  color="success">
    <h2 className="active">Registro de Clientes</h2>
    <CardBody >

<Form>
<Row form>
<Label sm={3}>Rut</Label>
<Col md={5}>
<FormGroup>
    <Input type="txt" name="rut" value={this.state.dataClientes.rut} required onChange={(e) => {
      let { dataClientes } = this.state;
      dataClientes.rut = e.target.value;
      this.setState({ dataClientes});
    }} placeholder="12345678" /> 
</FormGroup>
</Col>
<Label sm={1}>-</Label>
 <Col md={2}>
<FormGroup>
    <Input type="txt" name="dv" value={this.state.dataClientes.dv} onChange={(e) => {
      let { dataClientes } = this.state;
      dataClientes.dv = e.target.value;
      this.setState({ dataClientes});
    }}  placeholder="K" required/>
</FormGroup>
</Col>
<Label sm={3}>Nombre</Label>
 <Col md={8}>
<FormGroup>
    <Input type="txt" name="nombre" value={this.state.dataClientes.nombre} onChange={(e) => {
      let { dataClientes } = this.state;
      dataClientes.nombre = e.target.value;
      this.setState({ dataClientes});
    }}  placeholder="Nombre" required/>
</FormGroup>
</Col>
<Label sm={3}>Apellidos</Label>
 <Col md={4}>
<FormGroup>
    <Input type="txt" name="paterno"  value={this.state.dataClientes.paterno} onChange={(e) => {
      let { dataClientes } = this.state;
      dataClientes.paterno = e.target.value;
      this.setState({ dataClientes});
    }}  placeholder="Paterno" />
</FormGroup>
</Col>
<Col md={4}>
<FormGroup>
    <Input type="txt" name="materno"  value={this.state.dataClientes.materno} onChange={(e) => {
      let { dataClientes } = this.state;
      dataClientes.materno = e.target.value;
      this.setState({ dataClientes});
    }}  placeholder="Materno" required/>
</FormGroup>
</Col>
<Label sm={3}>Teléfono</Label>
 <Col md={8}>
<FormGroup>
    <Input type="txt" name="fono"  value={this.state.dataClientes.fono} onChange={(e) => {
      let { dataClientes } = this.state;
      dataClientes.fono = e.target.value;
      this.setState({ dataClientes});
    }}  placeholder="99999xxxx" required/>
</FormGroup>
</Col>
<Label sm={3}>Direción</Label>
 <Col md={8}>
<FormGroup>
    <Input type="txt" name="direccion"  value={this.state.dataClientes.direccion} onChange={(e) => {
      let { dataClientes } = this.state;
      dataClientes.direccion = e.target.value;
      this.setState({ dataClientes});
    }}  placeholder="Direción" required/>
</FormGroup>
</Col>
<Label sm={3}>Correo</Label>
 <Col md={8}>
<FormGroup>
    <Input type="txt" name="mail"  value={this.state.dataClientes.mail} onChange={(e) => {
      let { dataClientes } = this.state;
      dataClientes.mail = e.target.value;
      this.setState({ dataClientes});
    }}  placeholder="nombre@correo.com" required/>
</FormGroup>
</Col>
 <Col sm={{ size: 'auto', offset: 4 }}>
   <FormGroup >  
  
    <button type="button" 
    className="btn btn-success"
    onClick={this.actualizar.bind(this)}
    >Modificar</button>{' '}
    
    <button type="button" 
    className="btn btn-success"
    onClick={this.ingresar.bind(this)}
    >Registrar</button>{ ''}

   </FormGroup>
   </Col> 
   <Col >
   {this.state.mensage.visible ?
     <Alert color="primary" className="text-center">
     {this.state.mensage.texto}
     </Alert> :''} 
  </Col>
   </Row>
   </Form>

   </CardBody>
   </Card>

  </Col>

  <Col>
   <Card body outline  color="success">
  <h2 className="active">Lista de Clientes</h2>
   <Buscador/>
   

   <Table striped>
   <thead>
   <tr>
   <th>Nombre</th>
   <th>Apellidos</th>
   <th>Teléfono</th>
   <th>Estado</th>
   <th>Deuda</th>
   <th>Dirección</th>
   <th>Eliminar</th>
   <th>Modificar</th>
   </tr>
   </thead>
   <tbody>
   {clientes}
   </tbody>
   </Table>

   </Card>
  </Col> 
  </Row>
  
  

   </div>
   </Container>
   <Footer/>
   </div>
   )
 }}
 export default Clientes;