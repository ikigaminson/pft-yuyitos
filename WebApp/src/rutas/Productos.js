import React from 'react';
import axios from 'axios';
import { CardDeck , Card, CardHeader,CardBody, Row,Col,Container} from 'reactstrap';
import { Button, Label, Form, FormGroup,Input,Table,Alert} from 'reactstrap';
import HeaderAdmin from '../components/HeaderAdmin';
import TituloPagina from '../components/TituloPagina';
import Buscador from '../components/Buscador';
import Footer from '../components/Footer';

export default class Clientes extends React.Component{
  state = {
    url:'http://54.186.20.165:4567/producto',

    productos:[],
    proveedores:[],
    familias:[],

    dataClientes:{
      id: '',
      rut:'',
      dv:'',
      nombre:'',
      paterno:'',
      materno:'',
      fono:'',
      direccion:'',
      mail:'',
    },

    mensage:{
      texto:'',
      visible:false
    }
  }
componentWillMount() {
    this._refresh();
  }

ingresar(){

}
actualizar(){

}
cargarMod(){

}
eliminar(){

}
_refresh() {
   axios.get(this.state.url+'/all')
   .then((response) => {this.setState({productos: response.data})})

   axios.get('http://54.186.20.165:4567/proveedor/all')
   .then((response) => {this.setState({proveedores: response.data})})

   axios.get('http://54.186.20.165:4567/familia/all')
   .then((response) => {this.setState({familias: response.data})})
}
render(){
let productos = this.state.productos.map((producto) => {
    return (
      <tr key={producto.id}>

      <td>{producto.id}</td>
      <td>
      <Button color="danger" size="sm" 
      onClick={this.eliminar.bind(this, producto.id)}
      >Eliminar</Button>
      </td>
      <td>
      <Button color="success" size="sm" className="mr-2" 
      onClick={this.cargarMod.bind
      (this, producto.id,producto.nombre)}
      >Modificar</Button>
      </td>
      
      </tr>
      )
    });
let proveedores = this.state.proveedores.map((proveedor)=>{
      return(
          <option value={proveedor.id}>{proveedor.nombre}</option>
        )
 });
let familias = this.state.familias.map((familia)=>{
      return(
          <option value={familia.id}>{familia.nombre}</option>
        )
 });

  return (
    <div>
    <HeaderAdmin/>
    <Container fluid>
     <div>
       
  <TituloPagina titulo="PRODUCTOS"/>

   <Row>
    <Col sm={{ size: '4', offset: 0 }}>
    <Card body outline  color="success">
    <h2 className="active">Registro de Productos</h2>
    <CardBody >

<Form>
<Row form>
<Label sm={3}>Nombre</Label>
 <Col md={8}>
<FormGroup>
          <Input type="txt" name="email" id="exampleEmail" placeholder="Nombre" />
</FormGroup>
</Col>
<Label sm={3}>Código</Label>
 <Col md={8}>
<FormGroup>
          <Input type="txt" name="email" id="exampleEmail" placeholder="Código" />
</FormGroup>
</Col>
<Label sm={3}>Precio Compra</Label>
 <Col md={8}>
<FormGroup>
          <Input type="txt" name="email" id="exampleEmail" placeholder="Precio Compra" />
</FormGroup>
</Col>
<Label sm={3}>Precio Venta</Label>
 <Col md={8}>
<FormGroup>
          <Input type="txt" name="email" id="exampleEmail" placeholder="Precio Venta" />
 </FormGroup>
</Col>
<Label sm={3}>Stock</Label>
 <Col md={8}>
<FormGroup>      
          <Input type="txt" name="email" id="exampleEmail" placeholder="Stock" />
</FormGroup>
</Col>
<Label sm={3}>Proveedor</Label>
 <Col md={8}>
<FormGroup>          
              <select className="custom-select" >
          <option selected="0">Proveedor</option>
           {proveedores}
           </select>
</FormGroup>
</Col>
<Label sm={3}>Familia</Label>
 <Col md={8}>
<FormGroup> 
               <select className="custom-select">
          <option selected="0">Familia</option>
           {familias}
           </select>
</FormGroup>
</Col>
<Col sm={{ size: 'auto', offset: 4 }}>
   <FormGroup >  
  
    <button type="button" 
    className="btn btn-success"
    onClick={this.actualizar.bind(this)}
    >Modificar</button>{' '}
    
    <button type="button" 
    className="btn btn-success"
    onClick={this.ingresar.bind(this)}
    >Registrar</button>{ ''}

   </FormGroup>
   </Col> 
   <Col >
   {this.state.mensage.visible ?
     <Alert color="primary" className="text-center">
     {this.state.mensage.texto}
     </Alert> :''} 
  </Col>
   </Row>
   </Form>

   </CardBody>
   </Card>

  </Col>
 
  <Col>
   <Card body outline  color="success">
   <h2 className="active">Lista de Productos</h2>
  <Buscador/>

  <Table striped>
        <thead>
          <tr>

            <th>Nombre</th>
            <th>Precio</th>
            <th>Stock</th>
            <th>Proveedor</th>
            <th>Familia</th>
            <th>Eliminar</th>
            <th>Modificar</th>
          </tr>
        </thead>
        <tbody>
         {productos}
        </tbody>
      </Table>
   </Card>
  </Col> 
  </Row>

   </div>
   </Container>
   <Footer/>
   </div>
  )
}
}