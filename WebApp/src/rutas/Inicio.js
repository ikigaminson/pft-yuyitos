import React from 'react';
import logo from '../logo.png';
import TituloPagina from '../components/TituloPagina';
import HeaderAdmin from '../components/HeaderAdmin';
import Footer from '../components/Footer';
import { Container, Row, Col, Card } from 'reactstrap';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, Form, FormGroup } from 'reactstrap';
import {Bar} from 'react-chartjs-2';

export default class Inicio extends React.Component{
  constructor(props) {
        super(props);
        this.state = {
            modal: false,
            unmountOnClose: true,
            año:'',

 chartData:{labels: ['Enero','Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                    'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
           datasets:[{label:['venta max.'],
                      data:[
                        617598,
                        181045,
                        153060,
                        106519,
                        105162,
                        95072,
                        95072,
                        95072,
                        95072,
                        95072,],
                      backgroundColor:[
                        'rgba(255, 99, 132, 0.6)',
                        'rgba(54, 162, 235, 0.6)',
                        'rgba(255, 206, 86, 0.6)',
                        'rgba(75, 192, 192, 0.6)',
                        'rgba(153, 102, 255, 0.6)',
                        'rgba(255, 159, 64, 0.6)',
                        'rgba(255, 99, 132, 0.6)',
                        'rgba(255, 99, 132, 0.6)',
                        'rgba(255, 99, 132, 0.6)',
                        'rgba(255, 99, 132, 0.6)',
                        'rgba(255, 99, 132, 0.6)',
                        'rgba(255, 99, 132, 0.6)']
          }]
      }
        };

        this.toggle = this.toggle.bind(this);
        this.changeUnmountOnClose = this.changeUnmountOnClose.bind(this);
    }

  toggle() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }

    changeUnmountOnClose(e) {
        let value = e.target.value;
        this.setState({ unmountOnClose: JSON.parse(value) });
    }

render(){

	return (
    <div >
    <HeaderAdmin/>
		<Container fluid >
		<div >
       
  <TituloPagina titulo="INICIO" />
  <h3>Almacen Yuyitos</h3>
  <h5>El mejor almacen del barrio y con los mejores precios</h5>
  <hr className="my-4"/>
  <Row>
     <Col sm={{ size: '6', offset: 0 }}>
     <Card body outline  color="success">
     <Col>
   <h3>Informe mes </h3>
 
         <div className="form-group">
            <select value="0" className="custom-select">
      		<option selected="0">Seleccionar Mes</option>
      		<option value="1">Enero</option>
      		<option value="2">Febrero</option>
      		<option value="3">Marzo</option>
      		<option value="4">Abril</option>
      		<option value="5">Mayo</option>
      		<option value="6">Junio</option>
      		<option value="7">Julio</option>
      		<option value="8">Agosto</option>
      		<option value="9">Septiembre</option>
      		<option value="10">Octubre</option>
      		<option value="11">Noviembre</option>
      		<option value="12">Diciembre</option>
    		</select>
		</div>
		<div>
		<button type="submit" className="btn btn-success">Ver Informe</button>
		</div>

          </Col>
           
          <Col><h3>Estadisticas de Compras y ventas</h3>
          		 <div  className="form-group">
            <select value={this.state.año} className="custom-select" onChange={(e) => {
      let { año } = this.state;
      año = e.target.value;
      this.setState({ año});
      }}>
      		<option selected="0">Seleccionar Año</option>
      		<option value="1">2016</option>
      		<option value="2">2017</option>
      		<option value="3">2018</option>
      		<option value="4">2019</option>
      		
    		</select>
         <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} unmountOnClose={this.state.unmountOnClose}>
                    <ModalHeader toggle={this.toggle}>Comptas y ventas { this.state.año} </ModalHeader>
                    <ModalBody>
                       <Bar data={this.state.chartData} option={{}}/>
                        <Bar data={this.state.chartData} option={{}}/>
                    </ModalBody>
         </Modal>
		</div>
		<div>
		<button type="submit" onClick={this.toggle} className="btn btn-success">Ver Estadistica</button>
		</div>
          </Col>
         </Card>
        </Col> 
        <Col Col sm={{ size: '4', offset: 1 }}>
          <img src={logo}   height="350" width="350" background= "transparent"  />
        </Col>
     </Row>
  </div>
</Container>
<Footer/>
</div>
 )
}}