import React from 'react';
import axios from 'axios';
import { CardDeck , Card, CardHeader,CardBody, Row,Col,Container} from 'reactstrap';
import { Button, Label, Form, FormGroup,Input,Table,Alert} from 'reactstrap';
import HeaderAdmin from '../components/HeaderAdmin';
import TituloPagina from '../components/TituloPagina';
import Buscador from '../components/Buscador';
import Footer from '../components/Footer';

export default class Clientes extends React.Component{
  state = {
    url:'http://54.186.20.165:4567/products',

    pedidos:[],
    proveedores:[],
    productos:[],

    dataClientes:{
      id: '',
      rut:'',
      dv:'',
      nombre:'',
      paterno:'',
      materno:'',
      fono:'',
      direccion:'',
      mail:'',
    },

    mensage:{
      texto:'',
      visible:false
    }
  }
componentWillMount() {
    this._refresh();
  }

ingresar(){

}
actualizar(){

}
cargarMod(){

}
eliminar(){

}
_refresh() {
   axios.get(this.state.url)
   .then((response) => {this.setState({pedidos: response.data})})

   axios.get('http://54.186.20.165:4567/proveedor/all')
   .then((response) => {this.setState({proveedores: response.data})})

   axios.get('http://54.186.20.165:4567/productos/all')
   .then((response) => {this.setState({productos: response.data})})
}
render(){
let pedidos = this.state.pedidos.map((pedido) => {
    return (
      <tr key={pedido.id}>

      <td>{pedido.nombre}</td>
      <td>
      <Button color="danger" size="sm" 
      onClick={this.eliminar.bind(this, pedido.id)}
      >Eliminar</Button>
      </td>
      <td>
      <Button color="success" size="sm" className="mr-2" 
      onClick={this.cargarMod.bind
      (this, pedido.id,pedido.nombre)}
      >Modificar</Button>
      </td>
      
      </tr>
      )
    });
let proveedores = this.state.proveedores.map((proveedor)=>{
      return(
          <option value={proveedor.id}>{proveedor.nombre}</option>
        )
 });
let productos = this.state.productos.map((producto)=>{
      return(
          <option value={producto.id}>{producto.nombre}</option>
        )
 });

  return (
    <div>
    <HeaderAdmin/>
    <Container fluid>
     <div>
       
  <TituloPagina titulo="PEDIDOS"/>

   <Row>
    <Col sm={{ size: '5', offset: 0 }}>
    <Card body outline  color="success">
    <h2 className="active">Generar Pedido</h2>
    <CardBody >

<Form>
<Row form>
<Label sm={3}>Proveedor</Label>
 <Col md={8}>
<FormGroup>
           <select className="custom-select" >
          <option selected="0">Seleccionar Proveedor</option>
           {proveedores}
           </select>
</FormGroup>
</Col>
<Label sm={3}>Producto</Label>
 <Col md={8}>
<FormGroup>
          <select className="custom-select" >
          <option selected="0">Seleccionar Producto</option>
           {productos}
           </select>
</FormGroup>
</Col>
<Label sm={3}>Cantidad</Label>
 <Col md={8}>
<FormGroup>
          <Input type="txt" name="email" id="exampleEmail" placeholder="Cantidad" />
</FormGroup>
</Col>
<Label sm={3}>Valor </Label>
 <Col md={8}>
<FormGroup>
          <Input type="txt" name="email" id="exampleEmail" placeholder="Valor" />
 </FormGroup>
</Col>
<Label sm={3}>Fecha llegada</Label>
 <Col md={8}>
<FormGroup>      
          <Input type="txt" name="email" id="exampleEmail" placeholder="Fecha de llegada" />
</FormGroup>
</Col>
<Col sm={{ size: 'auto', offset: 6 }}>
   <FormGroup >  
  
    <button type="button" 
    className="btn btn-success"
    onClick={this.actualizar.bind(this)}
    >Modificar</button>{' '}
    
    <button type="button" 
    className="btn btn-success"
    onClick={this.ingresar.bind(this)}
    >Registrar</button>{ ''}

   </FormGroup>
   </Col> 
   <Col >
   {this.state.mensage.visible ?
     <Alert color="primary" className="text-center">
     {this.state.mensage.texto}
     </Alert> :''} 
  </Col>
   </Row>
   </Form>

   </CardBody>
   </Card>

  </Col>
 
  <Col>
   <Card body outline  color="success">
   <h2 className="active">Lista de Pedidos</h2>
  <Buscador/>

  <Table striped>
        <thead>
          <tr>

            <th>Proveedor</th>
            <th>Producto</th>
            <th>Cantidad</th>
            <th>Valor</th>
            <th>Eliminar</th>
            <th>Modificar</th>
          </tr>
        </thead>
        <tbody>
         {productos}
        </tbody>
      </Table>
   </Card>
  </Col> 
  </Row>

<div>
<Card body outline  color="success" className="mt-sm-4">
<Alert  className=" m-sm-0 text-center" color="red" >
<h2 className="active">Chequear orden</h2>
<Form className="mt-sm-2">
<Row form>

 <Col sm={{ size: '3', offset: 4 }}>
<FormGroup>
           <select className="custom-select" >
          <option selected="0">Seleccionar Fecha</option>
           {proveedores}
           </select>
</FormGroup>
</Col>
<FormGroup>
<button type="button" 
    className="btn btn-success"
    onClick={this.actualizar.bind(this)}
    >Ver órdenes</button>
</FormGroup>
</Row>
</Form>
 <Row>
<Col md={11}>
<Table striped>
        <thead>
          <tr>
            <th>Proveedor</th>
            <th>Producto</th>
            <th>Cantidad</th>
            <th>Chequeaado</th>
          </tr>
        </thead>
        <tbody>
          
        </tbody>
      </Table>
</Col>
<Col md={1}> <FormGroup>
<button type="button" 
    className="btn btn-success"
    onClick={this.actualizar.bind(this)}
    >Guardar </button>
</FormGroup>
</Col>
</Row>
</Alert>
</Card>
</div>

   </div>
   </Container>
   <Footer/>
   </div>
  )
}
}