import React from 'react';
import axios from 'axios';
import logo from '../logo.png';
import { Container, Row, Col,Card,CardBody } from 'reactstrap';
import TituloPagina from '../components/TituloPagina';
import { Button, Form, FormGroup, Label, Input, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import HeaderAdmin from '../components/HeaderAdmin';
import HeaderVendedor from '../components/HeaderVendedor';
import Footer from '../components/Footer';

export default class Clientes extends React.Component{
constructor(props) {
    super(props);

    const token = localStorage.getItem("ventas")
    let ventasIn = true

    if (token === null) {
    ventasIn = false
    }
    

    this.state = {
      codigoPro:'',
      cantidad:"",
      modal: false,
      ventasIn
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }
agrgarProd(){
      axios.post('')
    .then((response) => {})
}

render(){

const externalCloseBtn = <button className="close" style={{ position: 'absolute', top: '15px', right: '15px' }} onClick={this.toggle}>&times;</button>;
	
  return (
    <div>
    {this.state.ventasIn?
      <HeaderVendedor/>:<HeaderAdmin/>
    }
   
		<Container fluid>
		  
  <TituloPagina titulo="VENTAS"/>
  
 <Row>
 <Col sm={{ size: '5', offset: 1 }}>
 <Card body outline  color="success">
 <h2 className="text-center active">Ingresar el Producto</h2>
 <CardBody>
 <Form >
<Row Form>
          <Label sm={4}>Ingresar código</Label>
          <Col md={8}>
          <FormGroup >
          <Input type="txt" className=" text-center" name="codigo" value={this.state.codigoPro} onChange={(e) => {
            let { codigoPro } = this.state;
            codigoPro = e.target.value;
            this.setState({ codigoPro});
          }} placeholder="Código del producto" />
          </FormGroup>
          </Col>
          <Label sm={4}>Cantidad</Label>
          <Col md={8}>
          <FormGroup >
         <Input type="txt"  className=" text-center"  name="cantidad" value={this.state.cantidad} onChange={(e) => {
            let { cantidad } = this.state;
            cantidad = e.target.value;
            this.setState({ cantidad});
          }} placeholder="Cantidad" />
        </FormGroup>
          </Col>
          <Col sm={{ size: 'auto', offset: 6 }}>
         <FormGroup >
          <button type="button" className="btn btn-success" onClick={this.agrgarProd.bind(this)}>Agregar Producto</button>
          </FormGroup>
          </Col>
 </Row>
</Form>
</CardBody>
</Card>
</Col>
<Col sm={{ size: '1', offset: 1 }}>
          <img src={logo}   height="320" width="320" background= "transparent"  />
</Col>
</Row>





<div>
<hr className="my-4"/>
  <h2 className="active">Boleta y Pago</h2>
  <Row>
   <Col sm={{ size: 6, offset: 0 }}> 
   <Card body outline  color="success">
  <CardBody>
<Form>
  <FormGroup>
          <Label for="exampleText"><h3>Boleta Yuyitos</h3></Label>
          <Input type="textarea" name="text" id="exampleText" />
 </FormGroup>
<FormGroup>
<hr className="my-4"/>
<button type="button" className="btn btn-success">Generar Boleta</button>
</FormGroup>
</Form>
</CardBody>
</Card>
</Col>



<Col sm={{ size: 6, offset: 0 }}> 
<Card body outline  color="success">
  <CardBody>

<Form>
<FormGroup>
<Label ><h3>Pagar</h3></Label>
</FormGroup>

<FormGroup >
 <Label><h4>Total a pagar: $</h4></Label><Label name="precioTotal"></Label>
</FormGroup>

<hr className="my-4"/>

<FormGroup className="text-center">
<Label ><h4>Forma de Pago</h4></Label>
</FormGroup>

<FormGroup>
<button type="button" className="btn btn-success" onClick={this.toggle}>Efectivo</button>{' '}

<button type="button" className="btn btn-success" onClick={this.toggle}>Fiado</button> {' '}
<Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} external={externalCloseBtn}>
   
          <ModalBody>
         <FormGroup>
          <Input type="txt" className=" text-center" name="codigo" placeholder="Rut Cliente" />
          </FormGroup>
          <FormGroup>
          <Input type="txt" className=" text-center" name="codigo" placeholder="Valor Fiado" />
          </FormGroup>
          <blockquote className="blockquote text-center">
          <Button color="primary" onClick={this.toggle}>Agregar Fiado</Button>{' '}
          </blockquote>

            </ModalBody>
          <ModalFooter>
            <Button className="btn btn-secondary my-2 my-sm-0" onClick={this.toggle}>Cancelar</Button>
          </ModalFooter>
        </Modal>


<button type="button" className="btn btn-success">Tarjeta de Debito</button> {' '}

<button type="button" className="btn btn-success">Tarjeta de Credito</button>
</FormGroup>

</Form>
</CardBody>
</Card>
</Col>
</Row>

   </div>
  </Container>
  <Footer/>
  </div>
  )
}
}