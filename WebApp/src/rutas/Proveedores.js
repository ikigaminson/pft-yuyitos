import React from 'react';
import axios from 'axios';
import { CardDeck , Card, CardHeader,CardBody, Row,Col,Container} from 'reactstrap';
import { Button, Label, Form, FormGroup,Input,Table,Alert} from 'reactstrap';
import HeaderAdmin from '../components/HeaderAdmin';
import TituloPagina from '../components/TituloPagina';
import Buscador from '../components/Buscador';
import Footer from '../components/Footer';

export default class Proveedores extends React.Component{

  state = {
    url:'http://54.186.20.165:4567/proveedor',

    proveedores:[],
    rubros:[],

     dataProveedores:{
      id: '',
      nombre:'',
      rut:'',
      dv:'',
      phone:'',
      direccion:'',
      mail:'',
    },
     mensage:{
      texto:'',
      visible:false
    }
}

componentWillMount() {
    this._refresh();
  }

ingresar() {
    axios.post(this.state.url+'/insert', this.state.dataProveedores)

    .then((response) => {
      let { proveedores } = this.state;

      proveedores.push(response.data);
      this._refresh();

      this.setState({ proveedores, dataProveedores: {
        nombre:'',
        rut:'',
        dv:'',
        phone:'',
        direccion:'',
        mail:''

      },mensage:{
        texto:'Proveedor Agregado',
        visible:true}});
    })
    .catch(function (error) {
      console.log(error);
    });
}

actualizar() {
    let { nombre, rut, dv, phone, direccion, mail } = this.state.dataProveedores;

    axios.post(this.state.url+'/update/'+ this.state.dataProveedores.id, {
      nombre, rut, dv, phone, direccion, mail
    })
    .then((response) => {
      this._refresh();
      this.setState({ dataProveedores: {
        id:'',
        nombre:'',
        rut:'',
        dv:'',
        phone:'',
        direccion:'',
        mail:''

      },mensage:{
        texto:'Proveedor Actualizado',
        visible:true}});
    })
    .catch(e => console.log(e));
  }

cargarMod(id, nombre, rut, dv, phone, direccion, mail) {
    this.setState({dataProveedores: {id, nombre, rut, dv, phone, direccion, mail}}) 
}


eliminar(id) {
    axios.post(this.state.url+'/delete/'+ id)
    .then((response) => {
      this._refreshBooks();
      this.setState({mensage:{
      texto:'Proveedor Eliminado',
      visible:true}});
    })
    .catch(e => console.log(e)); 
}

_refresh() {
   axios.get(this.state.url+'/all')
   .then((response) => {this.setState({proveedores: response.data})})
   .catch(e => console.log(e));

   axios.get('http://54.186.20.165:4567/rubro/all')
   .then((response) => {this.setState({rubros: response.data})})
}

render(){

 let proveedores = this.state.proveedores.map((proveedor) => {
    return (
      <tr key={proveedor.id}>

      <td>{proveedor.nombre}</td>
      <td>{proveedor.rut}-{proveedor.dv}</td>
      <td>{proveedor.phone}</td>
      <td>{proveedor.direccion}</td>
      <td>{proveedor.email}</td>
      <td>
      <Button color="danger" size="sm" 
      onClick={this.eliminar.bind(this, proveedor.id)}
      >Eliminar</Button>
      </td>
      <td>
      <Button color="success" size="sm" className="mr-2" 
      onClick={this.cargarMod.bind
      (this, proveedor.id,proveedor.nombre,proveedor.rut,proveedor.dv,proveedor.phone,proveedor.direccion,proveedor.email)}
      >Modificar</Button>
      </td>
      </tr>
      )
    });
     let rubros = this.state.rubros.map((rubro)=>{
      return(
          <option value={rubro.id}>{rubro.nombre}</option>
        )
     });

	return (
    <div>
    <HeaderAdmin/>
		<Container fluid>
  <div>
  <TituloPagina titulo="PROVEEDORES"/>

   <Row>
     <Col sm={{ size: '4', offset: 0 }}>

    <Card body outline  color="success">
     <h2 className="active">Registro de Proveedores</h2>
    <CardBody >


   <Form>
   <Row form>
<Label sm={3}>Nombre</Label>
 <Col md={8}>
        <FormGroup>
          <Input type="txt" name="nombre" value={this.state.dataProveedores.nombre} onChange={(e) => {
            let { dataProveedores } = this.state;
            dataProveedores.nombre = e.target.value;
            this.setState({ dataProveedores});
          }} placeholder="Nombre" />
</FormGroup>
</Col>
<Label sm={3}>Rut Emp.</Label>
<Col md={5}>
<FormGroup>
          <Input type="txt" name="rut" value={this.state.dataProveedores.rut} onChange={(e) => {
            let { dataProveedores } = this.state;
            dataProveedores.rut = e.target.value;
            this.setState({ dataProveedores});
          }} placeholder="Rut Empresa" />
</FormGroup>
</Col>
<Label sm={1}>-</Label>
 <Col md={2}>
<FormGroup>
           <Input type="txt" name="dv" value={this.state.dataProveedores.dv} onChange={(e) => {
            let { dataProveedores } = this.state;
            dataProveedores.dv = e.target.value;
            this.setState({ dataProveedores});
          }} placeholder="K" />
</FormGroup>
</Col>
<Label sm={3}>Teléfono</Label>
 <Col md={8}>
<FormGroup>
           <Input type="txt" name="phone" value={this.state.dataProveedores.phone} onChange={(e) => {
            let { dataProveedores } = this.state;
            dataProveedores.phone = e.target.value;
            this.setState({ dataProveedores});
          }} placeholder="Teléfono" /> 
</FormGroup>
</Col>
<Label sm={3}>Direción</Label>
 <Col md={8}>
<FormGroup> 
          <Input type="txt" name="direccion" value={this.state.dataProveedores.direccion} onChange={(e) => {
            let { dataProveedores } = this.state;
            dataProveedores.direccion = e.target.value;
            this.setState({ dataProveedores});
          }} placeholder="Dirección" />
</FormGroup>
</Col>
<Label sm={3}>Correo</Label>
 <Col md={8}>
<FormGroup>       
         <Input type="txt" name="mail" value={this.state.dataProveedores.mail} onChange={(e) => {
            let { dataProveedores } = this.state;
            dataProveedores.mail = e.target.value;
            this.setState({ dataProveedores});
          }} placeholder="nombre@correo.com" />
</FormGroup>
</Col>
<Label sm={3}>Rubro</Label>
 <Col md={8}>
<FormGroup>          
           <select className="custom-select">
      		<option selected="0">Seleccionar Rubro</option>
            {rubros}
    		   </select>
</FormGroup>
</Col>
<Col sm={{ size: 'auto', offset: 4 }}>
  <FormGroup >  
       <button type="button" 
        className="btn btn-success"
        onClick={this.actualizar.bind(this)}
        >Modificar</button>{' '}

        <button type="button" 
        className="btn btn-success"
        onClick={this.ingresar.bind(this)}
        >Registrar</button>
  </FormGroup>     
</Col>
  <Col >
   {this.state.mensage.visible ?
     <Alert color="primary" className="text-center">
     {this.state.mensage.texto}
     </Alert> :''}       
  </Col>
  </Row>
</Form>
 </CardBody>
   </Card>
   </Col>
  
   <Col>
   <Card body outline  color="success">
  <h2 className="active">Lista de Proveedores</h2>
  <Buscador/>

  <Table striped>
        <thead>
        <th>Nombre</th>
        <th>Rut</th>
        <th>Teléfono</th>
        <th>Dirección</th>
        <th>Mail</th>
        <th>Eliminar</th>
        <th>Modificar</th>
        </thead>
        <tbody>
           {proveedores}
        </tbody>
      </Table>
 
   </Card>
  </Col> 
  </Row>
  
  

   </div>
   </Container>
   <Footer/>
   </div>
  )
}
}