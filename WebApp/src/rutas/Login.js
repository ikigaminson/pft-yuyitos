import React from 'react';
import '../css/Login.css';
import logo from '../logo.png';
import axios from 'axios';
import { Form, FormGroup, Input,Button   } from 'reactstrap';
import {Redirect}  from 'react-router-dom';



export default class Login extends React.Component{
constructor(props) {
  super(props);
  const tokenIn = localStorage.getItem("acept")
  const token = localStorage.getItem("ventas")
  let loggedIn = true 
  let ventasIn = true

  if (tokenIn === null) {
    loggedIn = false
  }
  if (token === null) {
    ventasIn = false
  }

  this.state = {
    dataUsuario:{
      user:'',
      pass:''
    },
    tipoUsuario:'',
    mensaje:'',
    showMensaje:false,
    loggedIn,
    ventasIn
    
  }
  this.consultar = this.consultar.bind(this);
  }
  



  consultar(e){
      e.preventDefault()

      axios.post('http://54.186.20.165:4567/usuarios/login?user='+ this.state.dataUsuario.user +'&pass=' + this.state.dataUsuario.pass)
     .then((response) => {
      if (response.data) {
        this._tipoUsuario()
        localStorage.setItem("acept","e")
        this.setState({loggedIn:true})
      }else{
        this.setState({mensaje:"Usuario y/o Contraseña no coinciden"})
        this.setState({showMensaje:true})
      }
     })
     .catch((e)=>{
      console.log(e);
      
    });  
  }

_tipoUsuario(){
  axios.post('http://54.186.20.165:4567/usuarios/search?rut='+ this.state.dataUsuario.user)
  .then((response) =>{
    this.setState({tipoUsuario:response.data.tipoUsuario.nombreTipo})
 if (this.state.tipoUsuario === 'VENDEDOR') {
    localStorage.setItem("ventas","i")
    this.setState({ventasIn:true})
    }
  })
}

render(){

     if (this.state.loggedIn){
      if (this.state.tipoUsuario === 'ADMIN') {
        return <Redirect to="/"/>
      }else if (this.state.tipoUsuario === 'VENDEDOR'){
        return <Redirect to="/Ventas"/>
      }}

	return (
<div className="body">
 
  <div className="wrapper fadeInDown" >
   <div id="formContent">
      <Form onSubmit={this.consultar}>
      
       <br/>
        <FormGroup>
          <center>
   <img src={logo} alt="Descripción de la imagen" height="100" width="100"  />
        </center>
        </FormGroup>

        {this.state.showMensaje?
          this.state.mensaje 
           :''}

          <FormGroup>
          <Input  name="user"  type="text"  placeholder="Usuario" 
          value={this.state.dataUsuario.user} onChange={(e) => {
      let { dataUsuario } = this.state;
      dataUsuario.user = e.target.value;
      this.setState({ dataUsuario});
      }}  required/>   
        </FormGroup>
        
        <FormGroup >
          <Input name="pass"  type="password" placeholder="Contraseña"
          value={this.state.dataUsuario.pass} onChange={(e) => {
      let { dataUsuario } = this.state;
      dataUsuario.pass = e.target.value;
      this.setState({ dataUsuario});
      }} required/>
        </FormGroup>
        <FormGroup>
        <Button color="success" >Iniciar Sesión</Button>
        </FormGroup>
        </Form>
        </div>    
        </div>
   
        </div>
		)
}

}