import React from 'react';

import Login from './Login';
import Inicio from './Inicio';
import Clientes from './Clientes';
import Ventas from './Ventas';
import Proveedores from './Proveedores';
import Pedidos from './Pedidos';
import Productos from './Productos';

import { BrowserRouter as Router, Route, Switch} from "react-router-dom";


export default class App extends React.Component{


render(){

	return (

       <Router>

       <Switch>
       <Route path="/Login" component={Login}/>
       <Route path="/Clientes" exact component={Clientes}/>
       <Route path="/Ventas" exact component={Ventas}/>
       <Route path="/Productos" exact component={Productos}/>
       <Route path="/Proveedores" exact component={Proveedores}/>
       <Route path="/Pedidos" exact component={Pedidos}/>
       <Route path="/Ventas" exact component={Ventas}/>
       <Route path="/" exact component={Inicio}/>
       </Switch>
       </Router>

		)


}



}

